import os
import numpy as np
from tensorflow.keras import backend as K
import matplotlib.pyplot as plt
import numpy.linalg as LA
import argparse
#import ESN
import importlib.util
spec = importlib.util.spec_from_file_location("ESN", "../ESN/ESN.py")
EchoStateNet = importlib.util.module_from_spec(spec)
spec.loader.exec_module(EchoStateNet)

parser = argparse.ArgumentParser()
parser.add_argument('-n','--normalization',help='Type of normalization', type=int)
parser.add_argument('-e','--epsilon',help='training error', type=float)
args = parser.parse_args()

#setup
epsilon = args.epsilon
normalization = args.normalization
# Read data from files
np_file = np.load('./KS_data.npz')
X = np_file['X']  # Data
dt = np_file['dt']
t_split = np_file['t_stop_train']
t_skip = np_file['t_skip']
val_ratio = np_file['val_ratio']
Lyap = np_file['Lyap']


#initial values
n_neurons = 2000
degree = 3.5  # average number of connections of a unit to other units in reservoir (3.0)
input_scaling = 0.3
rho = 0.4
leaking_rate = 1.0
beta = 0.00001

#dictionary for first parameter search
dict = {
    "input_scaling": [0.2, 0.3, 0.4, 0.5],
    "rho": [0,3, 0.4, 0.5, 0.6],
    "degree": [3.0, 3.5, 4.0],
    "leaking_rate": [0.9, 0.95, 1.0],
    "beta": [0.0001, 0.00005, 0.00001]
}

#dictionary for initial step size in parameter search (half of difference in parameters)
dict_step = {
    "input_scaling": 0.05,
    "rho": 0.05,
    "degree": 0.2,
    "leaking_rate": 0.02,
    "beta": 0.00002
}


# prepare data for training

# skip beginning
i_skip = int(t_skip / dt)
X = X[i_skip:, :]

# normalize data
stddev = np.std(X, axis=0)
avg = np.average(X, axis=0)

if normalization == 1:
    X = (X - avg) / stddev

if normalization == 2:
    X = X - avg
    maxi = np.max(np.abs(X),0)
    X = X / maxi

if normalization == 3:
    X = X - avg

input_all = X[:-1, :]
output_all = X[1:, :]

# split data into training, validation
idx_split = int(t_split / dt) - i_skip
assert idx_split > 0, 'skip time is bigger than split time'
# index that seperates training and validation data
idx_val = int(idx_split * (1 - val_ratio))

input_train = input_all[:idx_split, :]
output_train = output_all[:idx_split, :]

#training_time = 10
#training_idx = int(training_time/dt)
resync_time = 1
resync_idx = int(resync_time/(Lyap*dt))
washout = 100

#shorten training data
#input_train = input_train[:training_idx,:]
#output_train = output_train[:training_idx,:]

#reshape input
input_train = input_train.reshape((1,input_train.shape[0],input_train.shape[1]))
print(input_train.shape)

#create dict for best parameters
best_param = {}


file = open("KS_ESN_err_" + str(epsilon) + "_norm_" + str(normalization) +".txt", "a")

for name,val in dict.items():

    file.write("Start parameter search with parameter: " + name + "\n")
    print("Start parameter search with parameter: " + name)

    results = []
    results_std = []
    iter = 0

    for value in val:

        exec("%s = %f" % (name,value))

        #create model with given parameters
        sparseness = 1. - degree / (n_neurons - 1.)  # sparseness of W

        K.clear_session()
        rng = np.random.RandomState(1)

        model = EchoStateNet.ESN(n_neurons, 64, leaking_rate, rho, sparseness, input_scaling, rng, 'tanh', beta)

        #train model
        model.fit(input_train, output_train, washout)
        model.reset_states()

        print("model with: " + str(n_neurons) + " neurons, " + str(sparseness) + " sparseness, " +
                   str(leaking_rate) + " leaking_rate, " + str(rho) + " rho, " + str(input_scaling) + " input_scaling, "
                   + str(beta) + " beta")
        file.write("model with: " + str(n_neurons) + " neurons, " + str(sparseness) + " sparseness, " +
                   str(leaking_rate) + " leaking_rate, " + str(rho) + " rho, " + str(input_scaling) + " input_scaling, "
                   + str(beta) + " beta" + "\n")

        #reference solution for prediction section
        x_ref = X[idx_split:, :]

        t_pred = 10 / Lyap
        err_t = []

        # start prediction
        for i in range(50):

            idx_start = int(i * t_pred / dt)
            idx_end = int((i + 1) * t_pred / dt)

            x_init = X[idx_split + idx_start - resync_idx:idx_split + idx_start, :]

            assert idx_end < x_ref.shape[0], 't_pred too long'
            x_loc = x_ref[idx_start:idx_end, :]  # local reference solution


            # initialize reservoir
            x_init = x_init.reshape((1, x_init.shape[0], x_init.shape[1]))
            model.predict(x_init)

            Y = []

            y_last = x_loc[0, :].reshape((1, 1, x_loc.shape[1]))
            Y.append(x_loc[0, :].reshape((1, x_loc.shape[1])))

            print('start natural response')
            for i in range(x_loc.shape[0] - 1):
                Y.append(model.predict(y_last))
                y_last = Y[-1].reshape((1, 1, x_loc.shape[1]))
                print('[%d/%d] predicitions done' % ((i), x_loc.shape[0])) if i % 100 == 0 else None

            model.reset_states()

            Y = np.array(Y)
            Y = Y.reshape(Y.shape[0], Y.shape[2])

            # denormalize
            if normalization == 1:
                Y = Y * stddev + avg
                x_loc = x_loc * stddev + avg

            if normalization == 2:
                Y = Y * maxi + avg
                x_loc = x_loc * maxi + avg

            if normalization == 3:
                Y = Y + avg
                x_loc = x_loc + avg

            # calculate Error
            err = LA.norm(Y - x_loc, axis=1) / np.sqrt(
                np.average(np.square(LA.norm(x_loc, axis=1))))

            t = (np.argmax(err > epsilon) + 1) * dt
            err_t.append(t)

        err_t = np.array(err_t)
        print('mean:%f' % np.average(err_t))
        print('standarddeviation:%f' % np.std(err_t))

        #write to file
        file.write("mean:" + str(np.average(err_t)) + "\n")
        file.write("stddev:" + str(np.std(err_t)) + "\n")

        results.append(np.average(err_t))
        results_std.append(np.std(err_t))


    if iter == 0:
        # store and set best value
        idx = results.index(max(results))
        best_param[name] = val[idx]
        err_old = max(results)
        std_old = results_std[idx]
        exec("%s = %f" % (name, val[idx]))

        print("chose" + name + "=" + str(val[idx]) + "as best value")
        file.write("chose" + name + "=" + str(val[idx]) + "as best value" + "\n")

    if iter != 0 and max(results) > err_old:
        # store and set best value
        idx = results.index(max(results))
        best_param[name] = val[idx]
        err_old = max(results)
        std_old = results_std[idx]
        exec("%s = %f" % (name, val[idx]))

        print("chose" + name + "=" + str(val[idx]) + "as best value")
        file.write("chose" + name + "=" + str(val[idx]) + "as best value" + "\n")

    if iter != 0 and max(results) < err_old:
        print("Run: " + name + " ,no better value found.")
        file.write("Run: " + name + ", no better value found." + "\n")


    iter += 1


print(best_param)


####################################################################################
# Line Search

for name,val in dict_step.items():

    #maximum values
    max_iter = 14
    max_refinement = 4
    refinement_factor = 0.1

    #set initial best value
    best_val = best_param[name]


    for direction in [-1,1]:

        file.write("Start line search in direction: " + str(direction) + " with parameter: " + name + "\n")
        print("Start line search in direction: " + str(direction) + " with parameter: " + name)

        #initial values
        step = val
        exec("%s = %f" % (name, best_param[name]))
        refinement = 0
        iter = 0

        exec("%s = %s + %f * %d" % (name, name, step, direction))

        while refinement < max_refinement and iter < max_iter:


            # create model with given parameters
            sparseness = 1. - degree / (n_neurons - 1.)  # sparseness of W

            K.clear_session()
            rng = np.random.RandomState(1)

            model = EchoStateNet.ESN(n_neurons, 64, leaking_rate, rho, sparseness, input_scaling, rng, 'tanh', beta)

            # train model
            model.fit(input_train, output_train, washout)
            model.reset_states()

            print("model with: " + str(n_neurons) + " neurons, " + str(sparseness) + " sparseness, " +
                  str(leaking_rate) + " leaking_rate, " + str(rho) + " rho, " + str(input_scaling) + " input_scaling, "
                  + str(beta) + " beta")
            file.write("model with: " + str(n_neurons) + " neurons, " + str(sparseness) + " sparseness, " +
                       str(leaking_rate) + " leaking_rate, " + str(rho) + " rho, " + str(
                input_scaling) + " input_scaling, "
                       + str(beta) + " beta" + "\n")

            # reference solution for prediction section
            x_ref = X[idx_split:, :]

            t_pred = 10 / Lyap
            err_t = []

            # start prediction
            for i in range(50):

                idx_start = int(i * t_pred / dt)
                idx_end = int((i + 1) * t_pred / dt)

                x_init = X[idx_split + idx_start - resync_idx:idx_split + idx_start, :]

                assert idx_end < x_ref.shape[0], 't_pred too long'
                x_loc = x_ref[idx_start:idx_end, :]  # local reference solution

                # initialize reservoir
                x_init = x_init.reshape((1, x_init.shape[0], x_init.shape[1]))
                model.predict(x_init)

                Y = []

                y_last = x_loc[0, :].reshape((1, 1, x_loc.shape[1]))
                Y.append(x_loc[0, :].reshape((1, x_loc.shape[1])))

                print('start natural response')
                for i in range(x_loc.shape[0] - 1):
                    Y.append(model.predict(y_last))
                    y_last = Y[-1].reshape((1, 1, x_loc.shape[1]))
                    print('[%d/%d] predicitions done' % ((i), x_loc.shape[0])) if i % 100 == 0 else None

                model.reset_states()

                Y = np.array(Y)
                Y = Y.reshape(Y.shape[0], Y.shape[2])

                # denormalize
                if normalization == 1:
                    Y = Y * stddev + avg
                    x_loc = x_loc * stddev + avg

                if normalization == 2:
                    Y = Y * maxi + avg
                    x_loc = x_loc * maxi + avg

                if normalization == 3:
                    Y = Y + avg
                    x_loc = x_loc + avg

                # calculate Error
                err = LA.norm(Y - x_loc, axis=1) / np.sqrt(
                    np.average(np.square(LA.norm(x_loc, axis=1))))

                t = (np.argmax(err > epsilon) + 1) * dt
                err_t.append(t)

            err_t = np.array(err_t)
            print('mean:%f' % np.average(err_t))
            print('standarddeviation:%f' % np.std(err_t))

            # write to file
            file.write("mean:" + str(np.average(err_t)) + "\n")
            file.write("stddev:" + str(np.std(err_t)) + "\n")

            err_new = np.average(err_t)
            std_new = np.std(err_t)


            if err_new < err_old:

                step = step * refinement_factor
                refinement = refinement + 1
                exec("%s = %s + %f * %d" % (name, name, step, direction))

                print("Param %s: go again in this direction with smaller step size" % name)
                file.write("Param " + name + ": go again in this direction with smaller step size" + "\n")

            if err_new >= err_old:
                exec("best_val = %s" % name)

                exec("%s = %s + %f * %d" % (name, name, step, direction))

                print("Param %s: go further in this direction with same step size" % name)
                file.write("Param " + name + ": go further in this direction with same step size" + "\n")
                err_old = err_new
                std_old = std_new

            iter += 1



    #Set best value
    exec("%s = best_val" % name)
    #store it
    best_param[name] = best_val
    #exec(str(best_param[name]) + "= %s" % name)

    print("chose " + name + "=" + str(best_param[name]) + "as best value") #TODO writes wrong output
    file.write("chose " + name + "=" + str(best_param[name]) + "as best value" + "\n")

file.write("best parameters:" + "\n")
for name,val in best_param.items():

    file.write(name + " = " + str(val) + "\n")
    print(name + " = " + str(val))

file.write("average time:" + "\n")
file.write(str(err_old) + "\n")

print("average time:")
print(err_old)

file.write("stddev time:" + "\n")
file.write(str(std_old) + "\n")

print("stddev time:")
print(std_old)

file.close()
