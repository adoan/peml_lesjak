import os
import numpy as np
from tensorflow.keras import backend as K
import matplotlib.pyplot as plt
import numpy.linalg as LA
import argparse
#import ESN
import importlib.util
spec = importlib.util.spec_from_file_location("ESN", "../ESN/ESN.py")
EchoStateNet = importlib.util.module_from_spec(spec)
spec.loader.exec_module(EchoStateNet)


#setup
epsilon = 0.4
normalization = 1
# Read data from files
# Read data from files
np_file = np.load('./Kolmogorov_Re20.0_T2000_DT01.npz')
X = np_file['X']  # Data
dt = np_file['dt']
t_split = np_file['t_stop_train']
t_skip = np_file['t_skip']
val_ratio = np_file['val_ratio']
Lyap = np_file['Lyap']
coord = np_file['coord']
Re = np_file['Re']


#initial values
n_neurons = 5000
degree = 3.5  # average number of connections of a unit to other units in reservoir (3.0)
input_scaling = 0.1
rho = 0.3
leaking_rate = 1.05
beta = 0.00001


# prepare data for training

# skip beginning
i_skip = int(t_skip / dt)
X = X[i_skip:, :]

# normalize data
stddev = np.std(X, axis=0)
avg = np.average(X, axis=0)

if normalization == 1:
    X = (X - avg) / stddev

if normalization == 2:
    X = X - avg
    maxi = np.max(np.abs(X),0)
    X = X / maxi

if normalization == 3:
    X = X - avg

input_all = X[:-1, :]
output_all = X[1:, :]

# split data into training, validation
idx_split = int(t_split / dt) - i_skip
assert idx_split > 0, 'skip time is bigger than split time'
# index that seperates training and validation data
idx_val = int(idx_split * (1 - val_ratio))

input_train = input_all[:idx_split, :]
output_train = output_all[:idx_split, :]

#training_time = 10
#training_idx = int(training_time/dt)
resync_time = 1
resync_idx = int(resync_time/(Lyap*dt))
washout = 100

#shorten training data
#input_train = input_train[:training_idx,:]
#output_train = output_train[:training_idx,:]

#reshape input
input_train = input_train.reshape((1,input_train.shape[0],input_train.shape[1]))
print(input_train.shape)


#create model with given parameters
sparseness = 1. - degree / (n_neurons - 1.)  # sparseness of W

K.clear_session()
rng = np.random.RandomState(1)

model = EchoStateNet.ESN(n_neurons, input_train.shape[2], leaking_rate, rho, sparseness, input_scaling, rng, 'tanh', beta)

#train model
model.fit(input_train, output_train, washout)
model.reset_states()

print("model with: " + str(n_neurons) + " neurons, " + str(sparseness) + " sparseness, " +
           str(leaking_rate) + " leaking_rate, " + str(rho) + " rho, " + str(input_scaling) + " input_scaling, "
           + str(beta) + " beta")


#reference solution for prediction section
x_ref = X[idx_split:, :]

t_pred = 300 / Lyap
err_t = []

# start prediction
for i in range(1):

    idx_start = int(i * t_pred / dt)
    idx_end = int((i + 1) * t_pred / dt)

    x_init = X[idx_split + idx_start - resync_idx:idx_split + idx_start, :]

    assert idx_end < x_ref.shape[0], 't_pred too long'
    x_loc = x_ref[idx_start:idx_end, :]  # local reference solution


    # initialize reservoir
    x_init = x_init.reshape((1, x_init.shape[0], x_init.shape[1]))
    model.predict(x_init)

    Y = []

    y_last = x_loc[0, :].reshape((1, 1, x_loc.shape[1]))
    Y.append(x_loc[0, :].reshape((1, x_loc.shape[1])))

    print('start natural response')
    for i in range(x_loc.shape[0] - 1):
        Y.append(model.predict(y_last))
        y_last = Y[-1].reshape((1, 1, x_loc.shape[1]))
        print('[%d/%d] predicitions done' % ((i), x_loc.shape[0])) if i % 100 == 0 else None

    model.reset_states()

    Y = np.array(Y)
    Y = Y.reshape(Y.shape[0], Y.shape[2])

    # denormalize
    if normalization == 1:
        Y = Y * stddev + avg
        x_loc = x_loc * stddev + avg

    if normalization == 2:
        Y = Y * maxi + avg
        x_loc = x_loc * maxi + avg

    if normalization == 3:
        Y = Y + avg
        x_loc = x_loc + avg

    # calculate Error
    err = LA.norm(Y - x_loc, axis=1) / np.sqrt(
        np.average(np.square(LA.norm(x_loc, axis=1))))

    t = (np.argmax(err > epsilon) + 1) * dt
    err_t.append(t)


t_plot = 100
plt.plot(np.arange(int(t_plot/dt))*dt,err[0:int(t_plot/dt)])
plt.xlabel('t in [s]')
plt.ylabel('$E(t)$',rotation='horizontal')
plt.axvline((np.argmax(err > epsilon) + 1) * dt,color='r')
plt.gca().yaxis.set_label_coords(-0.1,0.5)
plt.show()

err_t = np.array(err_t)
print('mean:%f' % np.average(err_t))
print('standarddeviation:%f' % np.std(err_t))
