import os.path
import numpy as np
from tensorflow.keras import backend as K
import matplotlib.pyplot as plt
import numpy.linalg as LA
from scipy.fft import fft2, ifft2, fftfreq, fft, ifft
from matplotlib import cm
#import POD
import importlib.util
spec1 = importlib.util.spec_from_file_location("POD", "../POD.py")
propdec = importlib.util.module_from_spec(spec1)
spec1.loader.exec_module(propdec)



class ROM():

    def __init__(self,phi,coord,h,Re,fln=None):


        self.N = int((coord.shape[0]-1)/2)
        self.n_mod = phi.shape[1]
        n = 4
        self.Re = Re

        # physical grid
        self.dx = coord[1]-coord[0]
        xx, yy = np.meshgrid(coord, coord)
        fx = np.sin(n * yy)

        self.phi = phi
        #roll arrays
        self.phi_roll = self.roll(phi)

        self.fx_trfd = self.proj_fx(fx)

        if os.path.exists(fln) == False:
            print('No File found for given number of skipped modes')
            self.calc_der()
            self.calc_params(fln)
        else:
            print('File found, load parameters')
            file = np.load(fln)
            self.A = file['A']
            self.B = file['B']



        #ETDRK4 params
        self.h = h
        w, v = LA.eig(self.A)
        inv_v = LA.inv(v)

        E = np.exp(h * w)
        self.E = np.matmul(v, np.matmul(np.diag(E), inv_v))
        E_2 = np.exp(h * w / 2)
        self.E_2 = np.matmul(v, np.matmul(np.diag(E_2), inv_v))
        Q = 0
        phi1 = 0.0
        phi2 = 0.0
        phi3 = 0.0

        M = 32  # number of evaluation points in Cauchy integral

        for j in range(1, M + 1):
            arg = h * w + np.ones(w.shape[0]) * np.exp(2j * np.pi * (j - 0.5) / M)

            phi1 += 1.0 / arg * (np.exp(arg) - np.ones(w.shape[0]))
            phi2 += 1.0 / arg ** 2 * (np.exp(arg) - arg - np.ones(w.shape[0]))
            phi3 += 1.0 / arg ** 3 * (np.exp(arg) - 0.5 * arg ** 2 - arg - np.ones(w.shape[0]))
            Q += 2.0 / arg * (np.exp(0.5 * arg) - np.ones(w.shape[0]))

        phi1 = np.real(phi1 / M)
        phi1 = np.matmul(v, np.matmul(np.diag(phi1), inv_v))
        phi2 = np.real(phi2 / M)
        phi2 = np.matmul(v, np.matmul(np.diag(phi2), inv_v))
        phi3 = np.real(phi3 / M)
        phi3 = np.matmul(v, np.matmul(np.diag(phi3), inv_v))
        Q = np.real(Q / M)
        self.Q = np.matmul(v, np.matmul(np.diag(Q), inv_v))

        self.f1 = phi1 - 3 * phi2 + 4 * phi3
        self.f2 = 2 * phi2 - 4 * phi3
        self.f3 = -phi2 + 4 * phi3




    def NonLin(self, a):

        temp = np.tensordot(self.B,a, axes=([2,0]))
        temp = np.tensordot(a,temp, axes=([0,1]))

        return temp + self.fx_trfd

    def proj_fx(self, fx):

        fx_trfd = np.zeros((self.n_mod))

        for mode in range(self.n_mod):

            fx_trfd[mode] = np.sum(self.phi_roll[0,mode,:,:] * fx)

        return fx_trfd


    def integrate(self,u):

        #assert x.size == 3, 'Input needs 3 entries'

        Nu = self.NonLin(u)
        a = np.matmul(self.E_2, u) + self.h / 2 * np.matmul(self.Q, Nu)
        Na = self.NonLin(a)
        b = np.matmul(self.E_2, u) + self.h / 2 * np.matmul(self.Q, Na)
        Nb = self.NonLin(b)
        c = np.matmul(self.E_2, a) + self.h / 2 * np.matmul(self.Q, 2*Nb-Nu)
        Nc = self.NonLin(c)
        # update rule
        u = np.matmul(self.E,u) + self.h * np.matmul(self.f1, Nu) + self.h * np.matmul(self.f2, Na+Nb) + self.h * np.matmul(self.f3,Nc)

        return u


    def roll(self,phi):

        phi_new = np.zeros((2, self.n_mod, 2*self.N+1, 2*self.N+1))

        for n in range(self.n_mod):
            for v in range(2):
                for j in range(2*self.N+1):
                    for i in range(2*self.N+1):
                        phi_new[v, n, j, i] = phi[i+(2*self.N+1)*j+(2*self.N+1)*(2*self.N+1)*v, n]

        print('(%d/%d) steps done' % (n, self.n_mod)) if n % 100 == 0 and n != 0 else None

        return phi_new


    def calc_params(self,fln):

        self.B = np.zeros((self.n_mod,self.n_mod,self.n_mod))
        self.A = np.zeros((self.n_mod,self.n_mod))

        shear = 0.5*(self.phi_roll_x + np.transpose(self.phi_roll_x,axes=(4,1,2,3,0)))

        for k in range(self.n_mod):
            for m in range(self.n_mod):

                self.A[k,m] = np.sum(self.phi_roll_x[0,k,:,:,0] * shear[0,m,:,:,0])
                self.A[k,m] += np.sum(self.phi_roll_x[0,k,:,:,1] * shear[0,m,:,:,1])
                self.A[k,m] += np.sum(self.phi_roll_x[1,k,:,:,0] * shear[1,m,:,:,0])
                self.A[k,m] += np.sum(self.phi_roll_x[1,k,:,:,1] * shear[1,m,:,:,1])
                self.A[k,m] = -2/self.Re * self.A[k,m]

                for n in range(self.n_mod):

                    temp1 = self.phi_roll[0,m,:,:] * self.phi_roll_x[0,n,:,:,0] + self.phi_roll[1,m,:,:] * self.phi_roll_x[0,n,:,:,1]
                    temp2 = self.phi_roll[0,m,:,:] * self.phi_roll_x[1,n,:,:,0] + self.phi_roll[1,m,:,:] * self.phi_roll_x[1,n,:,:,1]
                    self.B[k,m,n] = -np.sum(self.phi_roll[0,k,:,:] * temp1)-np.sum(self.phi_roll[1,k,:,:] * temp2)

                    print('(%d/%d) in total' % ((k*self.n_mod*self.n_mod)+(m*self.n_mod)+n, self.n_mod**3)) if ((k*self.n_mod*self.n_mod)+(m*self.n_mod)+n) % 100000 == 0 else None

        #print('(%d/%d) modes done' % (k, self.n_mod)) if k % 3 == 0 else None

        print('save params to file')
        np.savez(fln, A=self.A, B=self.B)





    def calc_der(self):

        k_x = fftfreq(self.phi_roll.shape[3])*self.phi_roll.shape[3] * 1j
        k_y = fftfreq(self.phi_roll.shape[2])*self.phi_roll.shape[2] * 1j
        self.k = [k_x.reshape(1,k_x.shape[0]), k_y.reshape(k_y.shape[0],1)]

        print('Allocate Space for Arrays')
        self.phi_roll_x = np.zeros((self.phi_roll.shape[0], self.phi_roll.shape[1], self.phi_roll.shape[2], self.phi_roll.shape[3], 2))

        print('Start derivative calculation')

        for var in range(2):
            for mode in range(self.phi_roll.shape[1]):

                temp = fft2(self.phi_roll[var, mode, :, :])

                for dir1 in range(2):
                    self.phi_roll_x[var,mode,:,:,dir1] = np.real(ifft2(self.k[dir1] * temp))

                print('(%d/%d) modes done' % (mode, self.n_mod)) if mode % 5 == 0 and mode != 0 else None
            print('(%d/%d) variables done' % (var+1, 2))


    def calc_der_fd(self):

        print('Allocate Space for Arrays')
        self.phi_roll_x = np.zeros((self.phi_roll.shape[0], self.phi_roll.shape[1], self.phi_roll.shape[2], self.phi_roll.shape[3], 2))

        print('Start derivative calculation')

        for var in range(2):
            for mode in range(self.phi_roll.shape[1]):

                for dir1 in range(2):
                    self.phi_roll_x[var,mode,:,:,dir1] = self.iter_diff(self.phi_roll[var,mode,:,:],dir1)


                print('(%d/%d) modes done' % (mode, self.n_mod)) if mode % 5 == 0 and mode != 0 else None
            print('(%d/%d) variables done' % (var+1, 2))



    def iter_diff(self,data,dir):

        ddx = np.zeros(data.shape)

        if dir == 0:
            ddx[:,0] = (data[:,1] - data[:,-1])/(2*self.dx)
            ddx[:,-1] =(data[:,0] - data[:,-2])/(2*self.dx)

            for i in range(1,data.shape[1]-1):
                ddx[:,i] = (data[:,i+1] - data[:,i-1])/(2*self.dx)

        if dir == 1:
            ddx[0, :] = (data[1, :] - data[-1, :]) / (2 * self.dx)
            ddx[-1, :] = (data[0, :] - data[-2, :]) / (2 * self.dx)

            for j in range(1, data.shape[0] - 1):
                ddx[j, :] = (data[j + 1, :] - data[j - 1, :]) / (2 * self.dx)

        return ddx








#setup
epsilon = 0.4
skip_dim = 500#555
plot = True
t_stop = 300


# Read data from files
np_file = np.load('./Kolmogorov_Re20.0_T2000_DT01.npz')
X = np_file['X']  # Data
dt = np_file['dt']
t_split = np_file['t_stop_train']
t_skip = np_file['t_skip']
val_ratio = np_file['val_ratio']
Lyap = np_file['Lyap']
coord = np_file['coord']
Re = np_file['Re']


# skip beginning
i_skip = int(t_skip / dt)
X = X[i_skip:, :]


#Decomposition
pod = propdec.POD()
eig, a, phi = pod(X,False)

print(eig)
print(eig/np.sum(eig))
#plt.plot(eig/np.sum(eig))
#plt.show()

if skip_dim == None:
    skip_dim = -a.shape[1]

#reduce dimension
a_red = a[:,0:-skip_dim]
phi_red = phi[:,0:-skip_dim]

#mstep = 0.001
#generate ROM
fln = './A_B_' + str(skip_dim) + '.npz'
rom = ROM(phi_red,coord,dt,Re,fln)

# main loop
tt = np.arange(0,t_stop+dt,dt)
aa = np.zeros((tt.shape[0],a_red.shape[1]))
aa[0,:] = a_red[0,:]

for i in range(tt.shape[0]-1):

    aa[i+1,:] = rom.integrate(aa[i,:])

    print('(%d/%d) steps done' % (i, tt.shape[0]-1)) if i % 50 == 0 else None


# for i in range(tt.shape[0]-1):
#
#     a_int = aa[i,:]
#
#     for t in range(int(dt/mstep)):
#         a_int = rom.integrate(a_int)
#         print('(%d/%d) micro steps done while (%d/%d) timesteps done' % (t, int(dt / mstep),i, tt.shape[0])) if t % 10 == 0 else None
#
#
#     aa[i+1,:] = a_int

print('Time integration complete')
uu = np.matmul(aa,np.transpose(phi_red))





if plot == True:

    x_loc = X[:uu.shape[0],:]
    # calculate Error
    err = LA.norm(uu - x_loc, axis=1) / np.sqrt(
        np.average(np.square(LA.norm(x_loc, axis=1))))
    plt.plot(tt,err)
    plt.show()
    #print((np.argmax(err > epsilon) + 1) * dt * Lyap)