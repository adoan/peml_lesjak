import os
import numpy as np
from tensorflow.keras import backend as K
import matplotlib.pyplot as plt
import numpy.linalg as LA
from scipy.fft import fft2, ifft2, fftfreq, fft, ifft
from matplotlib import cm
#import POD
import importlib.util
spec1 = importlib.util.spec_from_file_location("POD", "../POD.py")
propdec = importlib.util.module_from_spec(spec1)
spec1.loader.exec_module(propdec)



class ROM():

    def __init__(self,phi,coord,h,Re):


        self.N = int((coord.shape[0]-1)/2)
        self.n_mod = phi.shape[1]
        n = 4
        self.Re = Re

        # physical grid
        self.dx = coord[1]-coord[0]
        xx, yy = np.meshgrid(coord, coord)
        fx = np.sin(n * yy)
        self.fx = self.unroll_fx(fx)

        self.phi = phi

        #roll arrays
        self.phi_roll = self.roll(phi)

        Lapl = self.calc_der_fd(self.phi_roll)
        Lapl = self.unroll_Lapl(Lapl)


        #ETDRK4 params
        self.h = h

        L = self.Lin(Lapl)
        w, v = LA.eig(L)
        inv_v = LA.inv(v)

        E = np.exp(h * w)
        self.E = np.matmul(v, np.matmul(np.diag(E), inv_v))
        E_2 = np.exp(h * w / 2)
        self.E_2 = np.matmul(v, np.matmul(np.diag(E_2), inv_v))
        Q = 0
        phi1 = 0.0
        phi2 = 0.0
        phi3 = 0.0

        M = 32  # number of evaluation points in Cauchy integral

        for j in range(1, M + 1):
            arg = h * w + np.ones(w.shape[0]) * np.exp(2j * np.pi * (j - 0.5) / M)

            phi1 += 1.0 / arg * (np.exp(arg) - np.ones(w.shape[0]))
            phi2 += 1.0 / arg ** 2 * (np.exp(arg) - arg - np.ones(w.shape[0]))
            phi3 += 1.0 / arg ** 3 * (np.exp(arg) - 0.5 * arg ** 2 - arg - np.ones(w.shape[0]))
            Q += 2.0 / arg * (np.exp(0.5 * arg) - np.ones(w.shape[0]))

        phi1 = np.real(phi1 / M)
        phi1 = np.matmul(v, np.matmul(np.diag(phi1), inv_v))
        phi2 = np.real(phi2 / M)
        phi2 = np.matmul(v, np.matmul(np.diag(phi2), inv_v))
        phi3 = np.real(phi3 / M)
        phi3 = np.matmul(v, np.matmul(np.diag(phi3), inv_v))
        Q = np.real(Q / M)
        self.Q = np.matmul(v, np.matmul(np.diag(Q), inv_v))

        self.f1 = phi1 - 3 * phi2 + 4 * phi3
        self.f2 = 2 * phi2 - 4 * phi3
        self.f3 = -phi2 + 4 * phi3


    def Lin(self,Lapl):

        return 1/self.Re * np.matmul(np.transpose(self.phi),Lapl)



    def NonLin(self, a):

        #calculate Leray projection term
        #print('calculate Leray terms')
        f = np.zeros((2*self.N+1, 2*self.N+1))
        p11 = np.zeros((2*self.N+1, 2*self.N+1))
        p12 = np.zeros((2*self.N+1, 2*self.N+1))
        p21 = np.zeros((2*self.N+1, 2*self.N+1))
        p22 = np.zeros((2*self.N+1, 2*self.N+1))
        p31 = np.zeros((2*self.N+1, 2*self.N+1))
        p41 = np.zeros((2*self.N+1, 2*self.N+1))
        p42 = np.zeros((2*self.N+1, 2*self.N+1))
        p51 = np.zeros((2*self.N+1, 2*self.N+1))
        p52 = np.zeros((2*self.N+1, 2*self.N+1))
        f_x = np.zeros((2, 2*self.N+1, 2*self.N+1))



        for k in range(2):
            for j in range(2):
                for i in range(2):
                    for mode in range(self.n_mod):

                        p11 += self.phi_roll_xxxx[k, mode, :, :, j, k, i, i] * a[mode]
                        p12 += self.phi_roll[j, mode, :, :] * a[mode]
                        p21 += self.phi_roll_x[k, mode, :, :, j] * a[mode]
                        p22 += self.phi_roll_xxx[j, mode, :, :, k, i, i] * a[mode]
                        p31 += self.phi_roll_xxx[i, mode, :, :, k, i, j] * a[mode]
                        p41 += self.phi_roll_xx[k,mode,:,:,j,i]* a[mode]
                        p42 += self.phi_roll_xx[j,mode,:,:,k,i]* a[mode]
                        p51 += self.phi_roll_xx[k, mode, :, :, j, k] * a[mode]
                        p52 += self.phi_roll_xx[j, mode, :, :, i, i] * a[mode]

                    f[:,:] += p11*p12 + 2*p21*p22 + 2*p21*p31 + 2*p41*p42 + p51*p52

                    p11.fill(0.0)
                    p12.fill(0.0)
                    p21.fill(0.0)
                    p22.fill(0.0)
                    p31.fill(0.0)
                    p41.fill(0.0)
                    p42.fill(0.0)
                    p51.fill(0.0)
                    p52.fill(0.0)


        #temp = fft2(f)
        for dir in range(2):
            f_x[dir, :, :] = self.iter_diff(f,dir)#np.real(ifft2(self.k[dir] * temp))

        #print('(%d/%d) modes done' % (mode, self.n_mod)) if mode % 5 == 0 and mode != 0 else None

        #create Leray projector
        f_x[0,:,:] = -1/f**2 * f_x[0,:,:]
        f_x[1,:,:] = -1/f**2 * f_x[1,:,:]


        # calculate convective term

        conv = np.zeros((2, 2 * self.N + 1, 2 * self.N + 1))

        for dir in range(2):
            for j in range(2):
                for mode in range(self.n_mod):

                    p11 += self.phi_roll[j,mode,:,:] * a[mode]
                    p12 += self.phi_roll_x[dir,mode,:,:,j] * a[mode]

                conv[dir,:,:] += p11*p12

                p11.fill(0.0)
                p12.fill(0.0)

        total = self.unroll_NL(-conv + f_x) + self.fx
        total = np.matmul(np.transpose(self.phi), total)

        return total.reshape((total.shape[0]))


    def integrate(self,u):

        #assert x.size == 3, 'Input needs 3 entries'

        Nu = self.NonLin(u)
        a = np.matmul(self.E_2, u) + self.h / 2 * np.matmul(self.Q, Nu)
        Na = self.NonLin(a)
        b = np.matmul(self.E_2, u) + self.h / 2 * np.matmul(self.Q, Na)
        Nb = self.NonLin(b)
        c = np.matmul(self.E_2, a) + self.h / 2 * np.matmul(self.Q, 2*Nb-Nu)
        Nc = self.NonLin(c)
        # update rule
        u = np.matmul(self.E,u) + self.h * np.matmul(self.f1, Nu) + self.h * np.matmul(self.f2, Na+Nb) + self.h * np.matmul(self.f3,Nc)

        return u


    def roll(self,phi):

        phi_new = np.zeros((2, self.n_mod, 2*self.N+1, 2*self.N+1))

        print('Flatten first array')
        for n in range(self.n_mod):
            for v in range(2):
                for j in range(2*self.N+1):
                    for i in range(2*self.N+1):
                        phi_new[v, n, j, i] = phi[i+(2*self.N+1)*j+(2*self.N+1)*(2*self.N+1)*v, n]

        print('(%d/%d) steps done' % (n, self.n_mod)) if n % 100 == 0 and n != 0 else None

        return phi_new

    def unroll_fx(self,fx_rolled):

        fx_new = np.zeros((2*fx_rolled.shape[0]*fx_rolled.shape[1],1))

        for i in range(fx_rolled.shape[0]):
            for j in range(fx_rolled.shape[1]):

                fx_new[j+i*fx_rolled.shape[1],0] = fx_rolled[i,j]

        return fx_new



    def unroll_P(self,P_rolled):

        P = np.zeros((P_rolled.shape[3]*P_rolled.shape[2]*P_rolled.shape[1],P_rolled.shape[0]))

        print('start unrolling P')
        for modes in range(P_rolled.shape[0]):
            for var in range(P_rolled.shape[3]):
                for i in range(P_rolled.shape[1]):
                    for j in range(P_rolled.shape[2]):
                        P[self.toidx(var,j,i),modes] = P_rolled[modes,i,j,var]

        return P

    def unroll_Lapl(self, Lapl_rolled):

        Lapl = np.zeros((Lapl_rolled.shape[0] * Lapl_rolled.shape[2] * Lapl_rolled.shape[3], Lapl_rolled.shape[1]))

        print('start unrolling Laplace')
        for modes in range(Lapl_rolled.shape[1]):
            for var in range(Lapl_rolled.shape[0]):
                for i in range(Lapl_rolled.shape[2]):
                    for j in range(Lapl_rolled.shape[3]):
                        Lapl[j+(2*self.N+1)*i+(2*self.N+1)*(2*self.N+1)*var, modes] = Lapl_rolled[var, modes, i, j]

        return Lapl

    def unroll_NL(self,N_roll):

        N_new = np.zeros((N_roll.shape[0]*N_roll.shape[1]*N_roll.shape[2],1))

        for var in range(N_roll.shape[0]):
            for i in range(N_roll.shape[1]):
                for j in range(N_roll.shape[2]):
                    N_new[j+(2*self.N+1)*i+(2*self.N+1)*(2*self.N+1)*var,0] = N_roll[var,i,j]

        return N_new




    def toidx(self,var,col,row):

        nr = 2*self.N+1
        var_nr = nr*nr

        return var_nr * var + row * nr + col

    def calc_der(self, phi_roll):

        k_x = fftfreq(phi_roll.shape[3])*phi_roll.shape[3] * 1j
        k_y = fftfreq(phi_roll.shape[2])*phi_roll.shape[2] * 1j
        self.k = [k_x.reshape(1,k_x.shape[0]), k_y.reshape(k_y.shape[0],1)]

        print('Allocate Space for Arrays')
        self.phi_roll_x = np.zeros((phi_roll.shape[0], phi_roll.shape[1], phi_roll.shape[2], phi_roll.shape[3], 2))
        self.phi_roll_xx = np.zeros((phi_roll.shape[0], phi_roll.shape[1], phi_roll.shape[2], phi_roll.shape[3], 2, 2))
        self.phi_roll_xxx = np.zeros((phi_roll.shape[0], phi_roll.shape[1], phi_roll.shape[2], phi_roll.shape[3], 2, 2, 2))
        self.phi_roll_xxxx = np.zeros((phi_roll.shape[0], phi_roll.shape[1], phi_roll.shape[2], phi_roll.shape[3], 2, 2, 2, 2))

        print('Start derivative calculation')

        for var in range(2):
            for mode in range(phi_roll.shape[1]):

                temp = fft2(phi_roll[var, mode, :, :])

                for dir1 in range(2):
                    self.phi_roll_x[var,mode,:,:,dir1] = np.real(ifft2(self.k[dir1] * temp))

                    for dir2 in range(2):
                        self.phi_roll_xx[var, mode, :, :, dir1, dir2] = np.real(ifft2(self.k[dir2] * self.k[dir1] * temp))

                        for dir3 in range(2):
                            self.phi_roll_xxx[var, mode, :, :, dir1, dir2, dir3] = np.real(ifft2(self.k[dir3] * self.k[dir2] * self.k[dir1] * temp))

                            for dir4 in range(2):
                                self.phi_roll_xxxx[var, mode, :, :, dir1, dir2, dir3, dir4] = np.real(ifft2(self.k[dir4] * self.k[dir3] * self.k[dir2] * self.k[dir1] * temp))


                print('(%d/%d) modes done' % (mode, self.n_mod)) if mode % 5 == 0 and mode != 0 else None
            print('(%d/%d) variables done' % (var+1, 2))


        #create Laplace operator
        Lapl = np.zeros((2,self.n_mod,2*self.N+1, 2*self.N+1))
        Lapl[0,:,:,:] = self.phi_roll_xx[0, :, :, :, 0, 0] + self.phi_roll_xx[0, :, :, :, 1, 1]
        Lapl[1,:,:,:] = self.phi_roll_xx[1, :, :, :, 0, 0] + self.phi_roll_xx[1, :, :, :, 1, 1]

        return Lapl

    def calc_der_fd(self, phi_roll):

        print('Allocate Space for Arrays')
        self.phi_roll_x = np.zeros((phi_roll.shape[0], phi_roll.shape[1], phi_roll.shape[2], phi_roll.shape[3], 2))
        self.phi_roll_xx = np.zeros((phi_roll.shape[0], phi_roll.shape[1], phi_roll.shape[2], phi_roll.shape[3], 2, 2))
        self.phi_roll_xxx = np.zeros((phi_roll.shape[0], phi_roll.shape[1], phi_roll.shape[2], phi_roll.shape[3], 2, 2, 2))
        self.phi_roll_xxxx = np.zeros((phi_roll.shape[0], phi_roll.shape[1], phi_roll.shape[2], phi_roll.shape[3], 2, 2, 2, 2))

        print('Start derivative calculation')

        for var in range(2):
            for mode in range(phi_roll.shape[1]):

                for dir1 in range(2):
                    self.phi_roll_x[var,mode,:,:,dir1] = self.iter_diff(phi_roll[var,mode,:,:],dir1)

                    for dir2 in range(2):
                        self.phi_roll_xx[var, mode, :, :, dir1, dir2] = self.iter_diff(self.phi_roll_x[var,mode,:,:,dir1],dir2)

                        for dir3 in range(2):
                            self.phi_roll_xxx[var, mode, :, :, dir1, dir2, dir3] = self.iter_diff(self.phi_roll_xx[var,mode,:,:,dir1,dir2],dir3)

                            for dir4 in range(2):
                                self.phi_roll_xxxx[var, mode, :, :, dir1, dir2, dir3, dir4] = self.iter_diff(self.phi_roll_xxx[var,mode,:,:,dir1,dir2,dir3],dir4)


                print('(%d/%d) modes done' % (mode, self.n_mod)) if mode % 5 == 0 and mode != 0 else None
            print('(%d/%d) variables done' % (var+1, 2))


        #create Laplace operator
        Lapl = np.zeros((2,self.n_mod,2*self.N+1, 2*self.N+1))
        Lapl[0,:,:,:] = self.phi_roll_xx[0, :, :, :, 0, 0] + self.phi_roll_xx[0, :, :, :, 1, 1]
        Lapl[1,:,:,:] = self.phi_roll_xx[1, :, :, :, 0, 0] + self.phi_roll_xx[1, :, :, :, 1, 1]

        return Lapl

    def iter_diff(self,data,dir):

        ddx = np.zeros(data.shape)

        if dir == 0:
            ddx[:,0] = (data[:,1] - data[:,-1])/(2*self.dx)
            ddx[:,-1] =(data[:,0] - data[:,-2])/(2*self.dx)

            for i in range(1,data.shape[1]-1):
                ddx[:,i] = (data[:,i+1] - data[:,i-1])/(2*self.dx)

        if dir == 1:
            ddx[0, :] = (data[1, :] - data[-1, :]) / (2 * self.dx)
            ddx[-1, :] = (data[0, :] - data[-2, :]) / (2 * self.dx)

            for j in range(1, data.shape[0] - 1):
                ddx[j, :] = (data[j + 1, :] - data[j - 1, :]) / (2 * self.dx)

        return ddx

    # def iter_y(self, data):
    #
    #     ddy = np.zeros(data.shape)
    #     ddy[0, :] = (data[1,:] - data[-1,:])/(2 * self.dx)
    #     ddy[-1,:] = (data[0,:] - data[-2,:])/(2 * self.dx)
    #
    #     for j in range(1, data.shape[0] - 1):
    #         ddy[j, :] = (data[j+1,:] - data[j-1,:])/(2*self.dx)
    #
    #     return ddy








#setup
epsilon = 0.4
skip_dim = 300#555
plot = False
t_stop = 1


# Read data from files
np_file = np.load('./Kolmogorov_Re20.0_T2000_DT01.npz')
X = np_file['X']  # Data
dt = np_file['dt']
t_split = np_file['t_stop_train']
t_skip = np_file['t_skip']
val_ratio = np_file['val_ratio']
Lyap = np_file['Lyap']
coord = np_file['coord']
Re = np_file['Re']


# skip beginning
i_skip = int(t_skip / dt)
X = X[i_skip:, :]


#Decomposition
pod = propdec.POD()
eig, a, phi = pod(X,False)

print(eig)
print(eig/np.sum(eig))
#plt.plot(eig/np.sum(eig))
#plt.show()

if skip_dim == None:
    skip_dim = -a.shape[1]

#reduce dimension
a_red = a[:,0:-skip_dim]
phi_red = phi[:,0:-skip_dim]

mstep = 0.001
#generate ROM
rom = ROM(phi_red,coord,mstep,Re)

# main loop
tt = np.arange(0,t_stop+dt,dt)
aa = np.zeros((tt.shape[0],a_red.shape[1]))
aa[0,:] = a_red[0,:]

# for i in range(tt.shape[0]-1):
#
#     aa[i+1,:] = rom.integrate(aa[i,:])
#
#     print('(%d/%d) steps done' % (i, tt.shape[0])) if i % 50 == 0 else None


for i in range(tt.shape[0]-1):

    a_int = aa[i,:]

    for t in range(int(dt/mstep)):
        a_int = rom.integrate(a_int)
        print('(%d/%d) micro steps done while (%d/%d) timesteps done' % (t, int(dt / mstep),i, tt.shape[0])) if t % 10 == 0 else None


    aa[i+1,:] = a_int

uu = np.matmul(aa,np.transpose(phi_red))





if plot == True:

    # calculate Error
    #err = LA.norm(x_pred - x_loc, axis=1) / np.sqrt(
    #    np.average(np.square(LA.norm(x_loc, axis=1))))
    #plt.plot(np.arange(0, x_loc.shape[0]) * dt * Lyap, err)
    #plt.show()
    #print((np.argmax(err > epsilon) + 1) * dt * Lyap)

    # plot
    coord, tt = np.meshgrid(coord, np.arange(0, aa.shape[0]) * dt * Lyap)

    # plt.contour(tt,x,uu,cmap=cm.coolwarm)
    plot = plt.pcolor(tt, coord, uu, cmap=cm.coolwarm)
    plt.show()