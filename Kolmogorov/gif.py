import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


np_file = np.load('./Kolmogorov_Re20.0_T2000_DT01.npz')
u_tot = np_file['u']# Data
v_tot = np_file['v']
dt = np_file['dt']

#writer = animation.ImageMagickWriter(fps=1000)

skip=7
t_gif=500
frps=10

ims_u = []

for i in range(0,t_gif,skip):
    im = plt.imshow(u_tot[:,:,i],interpolation='bicubic',cmap='coolwarm',animated=True, extent=[0,2*np.pi,0,2*np.pi],vmax=1,vmin=-1)
    ims_u.append([im])


ani = animation.ArtistAnimation(plt.gcf(), ims_u, interval=100, blit=True,
                                repeat_delay=1000)

ani.save('./u.gif',fps=frps, writer='imagemagick')

plt.clf()

ims_v = []

for i in range(0,t_gif,skip):
    im = plt.imshow(v_tot[:,:,i],interpolation='bicubic',cmap='coolwarm',animated=True, extent=[0,2*np.pi,0,2*np.pi],vmax=1,vmin=-1)
    ims_v.append([im])


ani = animation.ArtistAnimation(plt.gcf(), ims_v, interval=100, blit=True,
                                repeat_delay=1000)

ani.save('./v.gif',fps=frps, writer='imagemagick')