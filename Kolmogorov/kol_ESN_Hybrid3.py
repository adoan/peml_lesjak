import os
import numpy as np
from tensorflow.keras import backend as K
import matplotlib.pyplot as plt
import numpy.linalg as LA
from scipy.fft import fftfreq, fft2, ifft2
from matplotlib import cm
#import POD
import importlib.util
spec1 = importlib.util.spec_from_file_location("POD", "../POD.py")
propdec = importlib.util.module_from_spec(spec1)
spec1.loader.exec_module(propdec)

#import ESN
spec2 = importlib.util.spec_from_file_location("ESN", "../ESN/ESN_Hybrid.py")
EchoStateNet = importlib.util.module_from_spec(spec2)
spec2.loader.exec_module(EchoStateNet)



class ROM():

    def __init__(self,phi,coord,h,Re,fln=None):


        self.N = int((coord.shape[0]-1)/2)
        self.n_mod = phi.shape[1]
        n = 4
        self.Re = Re

        # physical grid
        self.dx = coord[1]-coord[0]
        xx, yy = np.meshgrid(coord, coord)
        fx = np.sin(n * yy)

        self.phi = phi
        #roll arrays
        self.phi_roll = self.roll(phi)

        self.fx_trfd = self.proj_fx(fx)

        if os.path.exists(fln) == False:
            print('No File found for given number of skipped modes')
            self.calc_der()
            self.calc_params(fln)
        else:
            print('File found, load parameters')
            file = np.load(fln)
            self.A = file['A']
            self.B = file['B']



        #ETDRK4 params
        self.h = h
        w, v = LA.eig(self.A)
        inv_v = LA.inv(v)

        E = np.exp(h * w)
        self.E = np.matmul(v, np.matmul(np.diag(E), inv_v))
        E_2 = np.exp(h * w / 2)
        self.E_2 = np.matmul(v, np.matmul(np.diag(E_2), inv_v))
        Q = 0
        phi1 = 0.0
        phi2 = 0.0
        phi3 = 0.0

        M = 32  # number of evaluation points in Cauchy integral

        for j in range(1, M + 1):
            arg = h * w + np.ones(w.shape[0]) * np.exp(2j * np.pi * (j - 0.5) / M)

            phi1 += 1.0 / arg * (np.exp(arg) - np.ones(w.shape[0]))
            phi2 += 1.0 / arg ** 2 * (np.exp(arg) - arg - np.ones(w.shape[0]))
            phi3 += 1.0 / arg ** 3 * (np.exp(arg) - 0.5 * arg ** 2 - arg - np.ones(w.shape[0]))
            Q += 2.0 / arg * (np.exp(0.5 * arg) - np.ones(w.shape[0]))

        phi1 = np.real(phi1 / M)
        phi1 = np.matmul(v, np.matmul(np.diag(phi1), inv_v))
        phi2 = np.real(phi2 / M)
        phi2 = np.matmul(v, np.matmul(np.diag(phi2), inv_v))
        phi3 = np.real(phi3 / M)
        phi3 = np.matmul(v, np.matmul(np.diag(phi3), inv_v))
        Q = np.real(Q / M)
        self.Q = np.matmul(v, np.matmul(np.diag(Q), inv_v))

        self.f1 = phi1 - 3 * phi2 + 4 * phi3
        self.f2 = 2 * phi2 - 4 * phi3
        self.f3 = -phi2 + 4 * phi3




    def NonLin(self, a):

        temp = np.tensordot(self.B,a, axes=([2,0]))
        temp = np.tensordot(a,temp, axes=([0,1]))

        return temp + self.fx_trfd

    def proj_fx(self, fx):

        fx_trfd = np.zeros((self.n_mod))

        for mode in range(self.n_mod):

            fx_trfd[mode] = np.sum(self.phi_roll[0,mode,:,:] * fx)

        return fx_trfd


    def integrate(self,u):

        #assert x.size == 3, 'Input needs 3 entries'

        Nu = self.NonLin(u)
        a = np.matmul(self.E_2, u) + self.h / 2 * np.matmul(self.Q, Nu)
        Na = self.NonLin(a)
        b = np.matmul(self.E_2, u) + self.h / 2 * np.matmul(self.Q, Na)
        Nb = self.NonLin(b)
        c = np.matmul(self.E_2, a) + self.h / 2 * np.matmul(self.Q, 2*Nb-Nu)
        Nc = self.NonLin(c)
        # update rule
        u = np.matmul(self.E,u) + self.h * np.matmul(self.f1, Nu) + self.h * np.matmul(self.f2, Na+Nb) + self.h * np.matmul(self.f3,Nc)

        return u


    def roll(self,phi):

        phi_new = np.zeros((2, self.n_mod, 2*self.N+1, 2*self.N+1))

        for n in range(self.n_mod):
            for v in range(2):
                for j in range(2*self.N+1):
                    for i in range(2*self.N+1):
                        phi_new[v, n, j, i] = phi[i+(2*self.N+1)*j+(2*self.N+1)*(2*self.N+1)*v, n]

        print('(%d/%d) steps done' % (n, self.n_mod)) if n % 100 == 0 and n != 0 else None

        return phi_new


    def calc_params(self,fln):

        self.B = np.zeros((self.n_mod,self.n_mod,self.n_mod))
        self.A = np.zeros((self.n_mod,self.n_mod))

        shear = 0.5*(self.phi_roll_x + np.transpose(self.phi_roll_x,axes=(4,1,2,3,0)))

        for k in range(self.n_mod):
            for m in range(self.n_mod):

                self.A[k,m] = np.sum(self.phi_roll_x[0,k,:,:,0] * shear[0,m,:,:,0])
                self.A[k,m] += np.sum(self.phi_roll_x[0,k,:,:,1] * shear[0,m,:,:,1])
                self.A[k,m] += np.sum(self.phi_roll_x[1,k,:,:,0] * shear[1,m,:,:,0])
                self.A[k,m] += np.sum(self.phi_roll_x[1,k,:,:,1] * shear[1,m,:,:,1])
                self.A[k,m] = -2/self.Re * self.A[k,m]

                for n in range(self.n_mod):

                    temp1 = self.phi_roll[0,m,:,:] * self.phi_roll_x[0,n,:,:,0] + self.phi_roll[1,m,:,:] * self.phi_roll_x[0,n,:,:,1]
                    temp2 = self.phi_roll[0,m,:,:] * self.phi_roll_x[1,n,:,:,0] + self.phi_roll[1,m,:,:] * self.phi_roll_x[1,n,:,:,1]
                    self.B[k,m,n] = -np.sum(self.phi_roll[0,k,:,:] * temp1)-np.sum(self.phi_roll[1,k,:,:] * temp2)

                    print('(%d/%d) in total' % ((k*self.n_mod*self.n_mod)+(m*self.n_mod)+n, self.n_mod**3)) if ((k*self.n_mod*self.n_mod)+(m*self.n_mod)+n) % 100000 == 0 else None

        #print('(%d/%d) modes done' % (k, self.n_mod)) if k % 3 == 0 else None

        print('save params to file')
        np.savez(fln, A=self.A, B=self.B)





    def calc_der(self):

        k_x = fftfreq(self.phi_roll.shape[3])*self.phi_roll.shape[3] * 1j
        k_y = fftfreq(self.phi_roll.shape[2])*self.phi_roll.shape[2] * 1j
        self.k = [k_x.reshape(1,k_x.shape[0]), k_y.reshape(k_y.shape[0],1)]

        print('Allocate Space for Arrays')
        self.phi_roll_x = np.zeros((self.phi_roll.shape[0], self.phi_roll.shape[1], self.phi_roll.shape[2], self.phi_roll.shape[3], 2))

        print('Start derivative calculation')

        for var in range(2):
            for mode in range(self.phi_roll.shape[1]):

                temp = fft2(self.phi_roll[var, mode, :, :])

                for dir1 in range(2):
                    self.phi_roll_x[var,mode,:,:,dir1] = np.real(ifft2(self.k[dir1] * temp))

                print('(%d/%d) modes done' % (mode, self.n_mod)) if mode % 5 == 0 and mode != 0 else None
            print('(%d/%d) variables done' % (var+1, 2))


    def calc_der_fd(self):

        print('Allocate Space for Arrays')
        self.phi_roll_x = np.zeros((self.phi_roll.shape[0], self.phi_roll.shape[1], self.phi_roll.shape[2], self.phi_roll.shape[3], 2))

        print('Start derivative calculation')

        for var in range(2):
            for mode in range(self.phi_roll.shape[1]):

                for dir1 in range(2):
                    self.phi_roll_x[var,mode,:,:,dir1] = self.iter_diff(self.phi_roll[var,mode,:,:],dir1)


                print('(%d/%d) modes done' % (mode, self.n_mod)) if mode % 5 == 0 and mode != 0 else None
            print('(%d/%d) variables done' % (var+1, 2))



    def iter_diff(self,data,dir):

        ddx = np.zeros(data.shape)

        if dir == 0:
            ddx[:,0] = (data[:,1] - data[:,-1])/(2*self.dx)
            ddx[:,-1] =(data[:,0] - data[:,-2])/(2*self.dx)

            for i in range(1,data.shape[1]-1):
                ddx[:,i] = (data[:,i+1] - data[:,i-1])/(2*self.dx)

        if dir == 1:
            ddx[0, :] = (data[1, :] - data[-1, :]) / (2 * self.dx)
            ddx[-1, :] = (data[0, :] - data[-2, :]) / (2 * self.dx)

            for j in range(1, data.shape[0] - 1):
                ddx[j, :] = (data[j + 1, :] - data[j - 1, :]) / (2 * self.dx)

        return ddx




#setup
epsilon = 0.4
skip_dim = 500
n_runs = 1
normalization = True

# Read data from files
np_file = np.load('./Kolmogorov_Re20.0_T2000_DT01.npz')
X = np_file['X']  # Data
dt = np_file['dt']
t_split = np_file['t_stop_train']
t_skip = np_file['t_skip']
val_ratio = np_file['val_ratio']
Lyap = np_file['Lyap']
coord = np_file['coord']
Re = np_file['Re']


#ESN setup
K.clear_session()
rng = np.random.RandomState(1)
n_neurons = 2000
degree = 3.0  # average number of connections of a unit to other units in reservoir (3.0)
sparseness = 1. - degree / (n_neurons - 1.)  # sparseness of W
input_scaling = 0.4
rho = 0.4
leaking_rate = 1.0
beta = 0.00005



# prepare data for training

# skip beginning
i_skip = int(t_skip / dt)
X = X[i_skip:, :]


#Decomposition
pod = propdec.POD()
eig, a, phi = pod(X,False)

print(eig)
print(eig/np.sum(eig))

if skip_dim == None:
    skip_dim = -a.shape[1]

#reduce dimension
a_red = a[:,0:-skip_dim]
phi_red = phi[:,0:-skip_dim]

#generate ROM
fln = './A_B_' + str(skip_dim) + '.npz'
rom = ROM(phi_red,coord,dt,Re,fln)

#create array with integrated ROM coefficients

a_rom = np.zeros((a.shape[0],a_red.shape[1]))
for i in range(a.shape[0]):

    a_rom[i,:] = rom.integrate(a_red[i,:])

    print('(%d/%d) steps done' % (i, a.shape[0])) if i % 50 == 0 else None


# calculate x_rom
x_rom = np.matmul(a_rom, np.transpose(phi_red))

# Concatenate real solution
input_all = np.concatenate((x_rom, X), axis=1)

# Create mapping
input_all = input_all[:-1, :]
output_all = X[1:, :]

#normalize for better prediction
if normalization == True:
    avg_i = np.average(input_all,axis=0)
    std_i = np.std(input_all, axis=0)
    avg_o = np.average(output_all,axis=0)
    std_o = np.std(output_all, axis=0)

    input_all = (input_all - avg_i) / std_i
    output_all = (output_all - avg_o) / std_o


# split data into training, validation
idx_split = int(t_split / dt) - i_skip
assert idx_split > 0, 'skip time is bigger than split time'
# index that seperates training and validation data
idx_val = int(idx_split * (1 - val_ratio))

input_train = input_all[:idx_split, :]
output_train = output_all[:idx_split, :]


#training_idx = int(training_time/dt)
resync_time = 1
resync_idx = int(resync_time/(Lyap*dt))
washout = 100


#shorten training data
#input_train = input_train[:training_idx,:]
#output_train = output_train[:training_idx,:]

#reshape input
input_train = input_train.reshape((1,input_train.shape[0],input_train.shape[1]))
print(input_train.shape)


#create model with given parameters

model = EchoStateNet.ESN_Hybrid(n_neurons, input_train.shape[2], leaking_rate, rho, sparseness, input_scaling, rng, 'tanh', beta)

#train model
model.fit(input_train, output_train, washout)
model.reset_states()


#reference solution for prediction section
x_ref = X[idx_split:, :]

t_pred = 300 / Lyap
err_t = []

# start prediction
for i in range(n_runs):

    idx_start = int(i * t_pred / dt)
    idx_end = int((i + 1) * t_pred / dt)

    x_init = input_all[idx_split + idx_start - resync_idx:idx_split + idx_start, :]

    assert idx_end < x_ref.shape[0], 't_pred too long'
    x_loc = x_ref[idx_start:idx_end, :]  # local reference solution


    # initialize reservoir
    x_init = x_init.reshape((1, x_init.shape[0], x_init.shape[1]))
    model.predict(x_init)

    x_pred = []

    x_last = input_all[idx_split + idx_start, :]
    x_last = x_last.reshape((1, 1, x_last.shape[0]))

    if normalization == True:
        print('start natural response')
        for i in range(x_loc.shape[0] - 1):
            # predict correction for next timestep
            x_pred.append(model.predict(x_last))

            # compute POD coefficients
            a_last = np.matmul(x_pred[-1] * std_o + avg_o, phi_red)

            # integrate last coefficients
            a_last = rom.integrate(a_last[0, :])

            # ROM state at next time step
            x_new = np.matmul(a_last, np.transpose(phi_red))
            x_new = (x_new - avg_i[0:x_new.shape[0]]) / std_i[0:x_new.shape[0]]


            # Form new input vector
            x_last = np.concatenate((x_new, x_pred[-1][0, :]), axis=0)

            # reshape x_last
            x_last = x_last.reshape(1, 1, x_last.shape[0])

            print('(%d/%d) predicitions done' % (i, x_loc.shape[0])) if i % 50 == 0 and i != 0 else None

    if normalization == False:

        print('start natural response')
        for i in range(x_loc.shape[0] - 1):
            # predict correction for next timestep
            x_pred.append(model.predict(x_last))

            # compute POD coefficients
            a_last = np.matmul(x_pred[-1], phi_red)

            # integrate last coefficients
            a_last = rom.integrate(a_last[0, :])

            # ROM state at next time step
            x_new = np.matmul(a_last, np.transpose(phi_red))

            # Form new input vector
            x_last = np.concatenate((x_new, x_pred[-1][0, :]), axis=0)

            # reshape x_last
            x_last = x_last.reshape(1, 1, x_last.shape[0])

            print('(%d/%d) predicitions done' % (i, x_loc.shape[0])) if i % 50 == 0 and i != 0 else None


    x_pred = np.array(x_pred)
    x_pred = x_pred.reshape(x_pred.shape[0], x_pred.shape[2])

    # add first timestep to array
    x_pred = np.vstack((output_all[idx_split + idx_start -1, :], x_pred))

    if normalization == True:
        x_pred = x_pred * std_o + avg_o


    model.reset_states()

    # calculate Error
    err = LA.norm(x_pred - x_loc, axis=1) / np.sqrt(
        np.average(np.square(LA.norm(x_loc, axis=1))))

    t = (np.argmax(err > epsilon) + 1) * dt
    err_t.append(t)


t_plot = 100
plt.plot(np.arange(int(t_plot/dt))*dt,err[0:int(t_plot/dt)])
plt.xlabel('t in [s]')
plt.ylabel('$E(t)$',rotation='horizontal')
plt.axvline((np.argmax(err > epsilon) + 1) * dt,color='r')
plt.gca().yaxis.set_label_coords(-0.1,0.5)
plt.show()


err_t = np.array(err_t)
print('mean:%f' % np.average(err_t))
print('standarddeviation:%f' % np.std(err_t))




