import os
import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as LA
#import ESN
import importlib.util
spec = importlib.util.spec_from_file_location("ESN", "/Users/ml/Documents/Studium/Masterarbeit/Code/pemlm/ESN/ESN.py")
EchoStateNet = importlib.util.module_from_spec(spec)
spec.loader.exec_module(EchoStateNet)

n_neurons = 500
rng = np.random.RandomState(1)
degree = 3.6  # average number of connections of a unit to other units in reservoir (3.0)
sparseness = 1. - degree / (n_neurons - 1.)  # sparseness of W

model = EchoStateNet.ESN(n_neurons, 5, 1.0, 0.47, sparseness, 0.37, rng, 'tanh',0.00003)


epsilon = 0.2
normalization = 0
# Read data from files
os.chdir("/Users/ml/Documents/Studium/Masterarbeit/Code/pemlm/PlattSys/")
np_file = np.load('./Platt_data.npz')
X = np_file['X']  # Data
dt = np_file['dt']
t_split = np_file['t_stop_train']
t_skip = np_file['t_skip']
val_ratio = np_file['val_ratio']
Lyap = np_file['Lyap']

# prepare data for training

# skip beginning
i_skip = int(t_skip / dt)
X = X[i_skip:, :]

# normalize data
stddev = np.std(X, axis=0)
avg = np.average(X, axis=0)

if normalization == 0:
    X = (X - avg) / stddev

if normalization == 1:
    X = X - avg
    max = np.array([np.amax(np.absolute(X[:, 0])), np.amax(np.absolute(X[:, 1])), np.amax(np.absolute(X[:, 2]))])
    X = X / max

input_all = X[:-1, :]
output_all = X[1:, :]

# split data into training, validation
idx_split = int(t_split / dt) - i_skip
assert idx_split > 0, 'skip time is bigger than split time'
# index that seperates training and validation data
idx_val = int(idx_split * (1 - val_ratio))

input_train = input_all[:idx_split, :]
output_train = output_all[:idx_split, :]

#training_time = 10
#training_idx = int(training_time/dt)
resync_time = 1
resync_idx = int(resync_time/dt)
washout = 100

#shorten training data
#input_train = input_train[:training_idx,:]
#output_train = output_train[:training_idx,:]

#reshape input
input_train = input_train.reshape((1,input_train.shape[0],input_train.shape[1]))
print(input_train.shape)

model.fit(input_train,output_train,washout)
model.reset_states()

#Teacher forced prediction
# Y = model.predict(input_train[:,washout:,:])
#
#
# plt.plot(input_train[0, washout:, 0])
# plt.plot(Y[:, 0], '--')
# plt.show()
#
# plt.plot(input_train[0, washout:, 1])
# plt.plot(Y[:, 1], '--')
# plt.show()
#
# plt.plot(input_train[0, washout:, 2])
# plt.plot(Y[:, 2], '--')
# plt.show()
#
# plt.plot(input_train[0, washout:, 3])
# plt.plot(Y[:, 3], '--')
# plt.show()
#
# plt.plot(input_train[0, washout:, 4])
# plt.plot(Y[:, 4], '--')
# plt.show()
#
# model.reset_states()



t_pred = 10 / Lyap
idx_end = int(t_pred / dt)
x_init = X[idx_split-resync_idx:idx_split-1,:]
x_ref = X[idx_split:, :]
assert idx_end < x_ref.shape[0], 't_pred too long'
x_ref = x_ref[:idx_end, :]

#initialize reservoir
x_init = x_init.reshape((1,x_init.shape[0],x_init.shape[1]))
model.predict(x_init)

Y = []

y_last = x_ref[0,:].reshape((1, 1, x_ref.shape[1]))
Y.append(x_ref[0,:].reshape((1,x_ref.shape[1])))

print('start natural response')
for i in range(x_ref.shape[0] - 1):
    Y.append(model.predict(y_last))
    y_last = Y[-1].reshape((1, 1, x_ref.shape[1]))
    print('[%d/%d] predicitions done' % ((i),x_ref.shape[0])) if i%100 == 0 else None

Y = np.array(Y)
Y = Y.reshape(Y.shape[0], Y.shape[2])


# denormalize
if normalization == 0:
    Y = Y * stddev + avg
    x_ref = x_ref * stddev + avg

if normalization == 1:
    Y = Y * max + avg
    x_ref = x_ref * max + avg

# calculate Error
err = LA.norm(Y[:, :] - x_ref[:, :], axis=1) / np.sqrt(
    np.average(np.square(LA.norm(x_ref[:, :], axis=1))))
plt.plot(err)
plt.show()

plt.plot(x_ref[:, 0])
plt.plot(Y[:, 0], '--')
plt.show()

plt.plot(x_ref[:, 1])
plt.plot(Y[:, 1], '--')
plt.show()

plt.plot(x_ref[:, 2])
plt.plot(Y[:, 2], '--')
plt.show()

plt.plot(x_ref[:, 3])
plt.plot(Y[:, 3], '--')
plt.show()

plt.plot(x_ref[:, 4])
plt.plot(Y[:, 4], '--')
plt.show()