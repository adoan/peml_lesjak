import numpy as np
from numpy import linalg as LA
from matplotlib import pyplot as plt

class PlattSys():

    nu_01 = 1.0 #0.0 for skew product structure
    nu_02 = 1.815
    nu_03 = 0.44
    nu_04 = 2.86 #0.0
    nu_05 = 2.86

    mu_01 = 1.815

    x1 = 0.0
    x2 = 1.0
    x3 = 1.0
    x4 = 1.0
    x5 = 1.0
    x0 = [[x1, x2, x3, x4, x5]]


    def dynamics(self, x):

        #assert x.size == 3, 'Input needs 3 entries'

        dxdt = np.zeros((5,))
        dxdt[0] = x[1]
        dxdt[1] = -x[0]**3 - 2*x[0]*x[2] + x[0]*x[4] - self.mu_01 * x[1]
        dxdt[2] = x[3]
        dxdt[3] = -x[2]**3 - self.nu_01 * x[0]**2 + x[4]*x[2] - self.nu_02 * x[3]
        dxdt[4] = -self.nu_03*x[4] - self.nu_04*x[0]*x[0] - self.nu_05*(x[2]*x[2]-1)

        return dxdt


    def jacobian(self, x):

        #assert x.size == 3, 'Input needs 3 entries'

        J = np.zeros((5,5))
        J[0,0] = 0.0
        J[0,1] = 1.0
        J[0,2] = 0.0
        J[0,3] = 0.0
        J[0,4] = 0.0
        J[1,0] = -3*x[0]-2*x[2]+x[4]
        J[1,1] = -self.mu_01
        J[1,2] = -2*x[0]
        J[1,3] = 0.0
        J[1,4] = x[0]
        J[2,0] = 0.0
        J[2,1] = 0.0
        J[2,2] = 0.0
        J[2,3] = 1.0
        J[2,4] = 0.0
        J[3,0] = -2*self.nu_01*x[0]
        J[3,1] = 0.0
        J[3,2] = -3*x[2]*x[2]+x[4]
        J[3,3] = -self.nu_02
        J[3,4] = x[2]
        J[4,0] = -2*self.nu_04*x[0]
        J[4,1] = 0.0
        J[4,2] = -2*self.nu_05*x[2]
        J[4,3] = 0.0
        J[4,4] = -self.nu_03

        return J

    def integrate(self,x,dt):

        #assert x.size == 3, 'Input needs 3 entries'

        k1 = self.dynamics(x)
        k2 = self.dynamics(x+dt/2.0*k1)
        k3 = self.dynamics(x+dt/2.0*k2)
        k4 = self.dynamics(x+dt*k3)

        return x + dt / 6.0 * (k1 + 2*k2 + 2*k3 + k4)


    def Lyapunovexp(self, solution, t_start, t_end, dt, interval):

        assert t_end/dt >= interval, 'interval is bigger than number of iterations'

        hypersphere = np.identity(5)
        y = np.zeros((5, 5))
        i = 0
        sum1, sum2, sum3 ,sum4, sum5 = 0.0 , 0.0 , 0.0, 0.0, 0.0

        while i < t_end/dt:

            k1 = np.matmul(self.jacobian(solution[i,:]),hypersphere)
            inter_sol = self.integrate(solution[i,:],dt/2)
            k2 = np.matmul(self.jacobian(inter_sol), hypersphere + dt / 2.0 * k1)
            k3 = np.matmul(self.jacobian(inter_sol), hypersphere + dt / 2.0 * k2)
            k4 = np.matmul(self.jacobian(solution[i+1, :]), hypersphere + dt * k3)

            z = hypersphere + dt / 6.0 * (k1 + 2 * k2 + 2 * k3 + k4)

            if (i%interval == 0 and i!=0 ) or i == t_end/dt-1:
                # gram_schmidt
                y[:,0] = z[:,0]
                length1 = LA.norm(y[:,0])
                y[:,1] = z[:,1] - np.dot(z[:,1], y[:,0] / length1) * y[:,0] / length1
                length2 = LA.norm(y[:,1])
                y[:,2] = z[:,2] - np.dot(z[:,2], y[:,0] / length1) * y[:,0] / length1 \
                          - np.dot(z[:,2], y[:,1] / length2) * y[:,1] / length2
                length3 = LA.norm(y[:, 2])
                y[:,3] = z[:,3] - np.dot(z[:,3], y[:,0] / length1) * y[:,0] / length1 \
                          - np.dot(z[:,3], y[:,1] / length2) * y[:,1] / length2 \
                          - np.dot(z[:,3], y[:,2] / length3) * y[:,2] / length3
                length4 = LA.norm(y[:,3])
                y[:,4] = z[:,4] - np.dot(z[:,4], y[:,0] / length1) * y[:,0] / length1 \
                          - np.dot(z[:,4], y[:,1] / length2) * y[:,1] / length2 \
                          - np.dot(z[:,4], y[:,2] / length3) * y[:,2] / length3 \
                          - np.dot(z[:,4], y[:,3] / length4) * y[:,3] / length4
                length5 = LA.norm(y[:,4])

                print('gram schmidt done at i= %d' %(i))

                # normalize Base and make new hypersphere
                hypersphere[:, 0] = y[:, 0] / length1
                hypersphere[:, 1] = y[:, 1] / length2
                hypersphere[:, 2] = y[:, 2] / length3
                hypersphere[:, 3] = y[:, 3] / length4
                hypersphere[:, 4] = y[:, 4] / length5

                sum1 += np.log(length1)
                sum2 += np.log(length2)
                sum3 += np.log(length3)
                sum4 += np.log(length4)
                sum5 += np.log(length5)

            else:
                hypersphere = z

            i+=1

        # ref Values: Lyapunov exponents in constrained and unconstrained ordinary differential equations
        lambda1 = sum1 / t_end
        lambda2 = sum2 / t_end
        lambda3 = sum3 / t_end
        lambda4 = sum4 / t_end
        lambda5 = sum5 / t_end

        return np.array([lambda1, lambda2, lambda3, lambda4, lambda5])


def main():

    save = 1
    plot = 0
    Lyapunov_calc = 0

    PSys = PlattSys()

    dt = 0.01
    Lyap = 0.11  # biggest positive Lyapunov exponent
    t_start = 0
    t_train = 100/Lyap
    t_skip = 500
    val_ratio = 0.8
    t_stop_train = t_train / val_ratio + t_skip  # assuming val=0.8 and tskip=1
    N = 100  # number of tests
    # t_stop is the end time (training + validation + reference for all N simulations)
    t_stop = t_stop_train + N * 10 / Lyap  # 10 is approx 10 Lyap times

    X = PSys.x0

#integration
    print('start integration loop')
    for t , val in np.ndenumerate(np.arange(t_start,t_stop,dt)):
        X.append(PSys.integrate(X[t[0]],dt))

    X = np.array(X)
    print(X.shape)


    if Lyapunov_calc == 1:
        print('start Lyapunov calc')
        lambdas = PSys.Lyapunovexp(X, t_start, t_stop, dt, 10) #50 Unit discs
        print(lambdas)
        print('trace of Jacobian: exact:-4.07, calculated:%.5f'%(np.sum(lambdas)))

    # save data
    if save == 1:
       np.savez('Platt_data',X=X,dt=dt,t_stop_train=t_stop_train,t_skip=t_skip, val_ratio=val_ratio,Lyap=Lyap)

    ## plot trajectory in plane
    if plot == 1:
        plt.plot(X[:,0],'b',linewidth='0.3',label='x1')
        #plt.plot(X[:,1],'g', linewidth='0.3',label='x2')
        #plt.plot(X[:,2],'r', linewidth='0.3',label='x3')
        #plt.plot(X[:,3],'m', linewidth='0.3',label='x4')
        #plt.plot(X[:,4],'c', linewidth='0.3',label='x5')
        #plt.savefig('Lorenz_attractor.pdf',format='pdf')
        #plt.legend()
        plt.show()


if __name__ == '__main__':
    main()