import os
import numpy as np
from tensorflow.keras import backend as K
import matplotlib.pyplot as plt
import numpy.linalg as LA
from scipy.fft import fft, ifft, fftfreq
from matplotlib import cm
#import POD
import importlib.util
spec1 = importlib.util.spec_from_file_location("POD", "../POD.py")
propdec = importlib.util.module_from_spec(spec1)
spec1.loader.exec_module(propdec)





class ROM():

    def __init__(self,phi,coord,h):

        length = np.amax(coord)
        N = coord.shape[0]

        k = fftfreq(N) * N * 2*np.pi/length
        im_k = 1j*k
        im_k = im_k.reshape(im_k.shape[0],1)

        self.phi = phi
        self.phix = np.real(ifft( im_k * fft(phi, axis=0), axis=0))
        self.phixx = np.real(ifft( im_k**2 * fft(phi, axis=0), axis=0))
        self.phixxxx =  np.real(ifft( im_k**4 * fft(phi,axis=0),axis=0))

        #ETDRK4 params
        self.h = h

        L = np.matmul(-np.transpose(self.phi),self.phixxxx + self.phixx)
        w, v = LA.eig(L)
        inv_v = LA.inv(v)

        E = np.exp(h * w)
        self.E = np.matmul(v, np.matmul(np.diag(E), inv_v))
        E_2 = np.exp(h * w / 2)
        self.E_2 = np.matmul(v, np.matmul(np.diag(E_2), inv_v))
        Q = 0
        phi1 = 0.0
        phi2 = 0.0
        phi3 = 0.0

        M = 32  # number of evaluation points in Cauchy integral

        for j in range(1, M + 1):
            arg = h * w + np.ones(w.shape[0]) * np.exp(2j * np.pi * (j - 0.5) / M)

            phi1 += 1.0 / arg * (np.exp(arg) - np.ones(w.shape[0]))
            phi2 += 1.0 / arg ** 2 * (np.exp(arg) - arg - np.ones(w.shape[0]))
            phi3 += 1.0 / arg ** 3 * (np.exp(arg) - 0.5 * arg ** 2 - arg - np.ones(w.shape[0]))
            Q += 2.0 / arg * (np.exp(0.5 * arg) - np.ones(w.shape[0]))

        phi1 = np.real(phi1 / M)
        phi1 = np.matmul(v, np.matmul(np.diag(phi1), inv_v))
        phi2 = np.real(phi2 / M)
        phi2 = np.matmul(v, np.matmul(np.diag(phi2), inv_v))
        phi3 = np.real(phi3 / M)
        phi3 = np.matmul(v, np.matmul(np.diag(phi3), inv_v))
        Q = np.real(Q / M)
        self.Q = np.matmul(v, np.matmul(np.diag(Q), inv_v))

        self.f1 = phi1 - 3 * phi2 + 4 * phi3
        self.f2 = 2 * phi2 - 4 * phi3
        self.f3 = -phi2 + 4 * phi3


    def NonLin(self, a):


        frac2 = np.matmul(self.phi,a) * np.matmul(self.phix,a)

        return -np.matmul(np.transpose(self.phi),frac2)


    def integrate(self,u):

        #assert x.size == 3, 'Input needs 3 entries'

        Nu = self.NonLin(u)
        a = np.matmul(self.E_2, u) + self.h / 2 * np.matmul(self.Q, Nu)
        Na = self.NonLin(a)
        b = np.matmul(self.E_2, u) + self.h / 2 * np.matmul(self.Q, Na)
        Nb = self.NonLin(b)
        c = np.matmul(self.E_2, a) + self.h / 2 * np.matmul(self.Q, 2*Nb-Nu)
        Nc = self.NonLin(c)
        # update rule
        u = np.matmul(self.E,u) + self.h * np.matmul(self.f1, Nu) + self.h * np.matmul(self.f2, Na+Nb) + self.h * np.matmul(self.f3,Nc)

        return u




#setup
epsilon = 0.4
skip_dim = 48
plot = True
t_stop = 300
mstep = 0.001

# Read data from files
np_file = np.load('./KS_data.npz')
X = np_file['X']  # Data
dt = np_file['dt']
t_split = np_file['t_stop_train']
t_skip = np_file['t_skip']
val_ratio = np_file['val_ratio']
Lyap = np_file['Lyap']
coord = np_file['coord']



# skip beginning
i_skip = int(t_skip / dt)
X = X[i_skip:, :]


#Decomposition
pod = propdec.POD()
eig, a, phi = pod(X,False)

print(eig)
print(eig/np.sum(eig))

#reduce dimension
a_red = a[:,0:-skip_dim]
phi_red = phi[:,0:-skip_dim]

#generate ROM
rom = ROM(phi_red,coord,mstep) #dt

# main loop
aa = a_red[0,:]
a_int = a_red[0,:]
tt = [0]

for t in np.arange(dt,t_stop+dt,dt):

    for i in range(int(dt/mstep)):
        a_int = rom.integrate(a_int)


    aa = np.vstack((aa,a_int))
    tt.append(t)

uu = np.matmul(aa,np.transpose(phi_red))





if plot == True:

    # calculate Error
    #err = LA.norm(x_pred - x_loc, axis=1) / np.sqrt(
    #    np.average(np.square(LA.norm(x_loc, axis=1))))
    #plt.plot(np.arange(0, x_loc.shape[0]) * dt * Lyap, err)
    #plt.show()
    #print((np.argmax(err > epsilon) + 1) * dt * Lyap)

    # plot
    coord, tt = np.meshgrid(coord, np.arange(0, aa.shape[0]) * dt * Lyap)

    # plt.contour(tt,x,uu,cmap=cm.coolwarm)
    plot = plt.pcolor(tt, coord, uu, cmap=cm.coolwarm)
    plt.show()





