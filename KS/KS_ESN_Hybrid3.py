import os
import numpy as np
from tensorflow.keras import backend as K
import matplotlib.pyplot as plt
import numpy.linalg as LA
from scipy.fft import fft, ifft, fftfreq
from matplotlib import cm
#import POD
import importlib.util
spec1 = importlib.util.spec_from_file_location("POD", "../POD.py")
propdec = importlib.util.module_from_spec(spec1)
spec1.loader.exec_module(propdec)

#import ESN
spec2 = importlib.util.spec_from_file_location("ESN", "../ESN/ESN_Hybrid.py")
EchoStateNet = importlib.util.module_from_spec(spec2)
spec2.loader.exec_module(EchoStateNet)



class ROM():

    def __init__(self,phi,coord,h):

        length = np.amax(coord)
        N = coord.shape[0]

        k = fftfreq(N) * N * 2*np.pi/length
        im_k = 1j*k
        im_k = im_k.reshape(im_k.shape[0],1)

        self.phi = phi
        self.phix = np.real(ifft( im_k * fft(phi, axis=0), axis=0))
        self.phixx = np.real(ifft( im_k**2 * fft(phi, axis=0), axis=0))
        self.phixxxx =  np.real(ifft( im_k**4 * fft(phi,axis=0),axis=0))

        #ETDRK4 params
        self.h = h

        L = np.matmul(-np.transpose(self.phi),self.phixxxx + self.phixx)
        w, v = LA.eig(L)
        inv_v = LA.inv(v)

        E = np.exp(h * w)
        self.E = np.matmul(v, np.matmul(np.diag(E), inv_v))
        E_2 = np.exp(h * w / 2)
        self.E_2 = np.matmul(v, np.matmul(np.diag(E_2), inv_v))
        Q = 0
        phi1 = 0.0
        phi2 = 0.0
        phi3 = 0.0

        M = 32  # number of evaluation points in Cauchy integral

        for j in range(1, M + 1):
            arg = h * w + np.ones(w.shape[0]) * np.exp(2j * np.pi * (j - 0.5) / M)

            phi1 += 1.0 / arg * (np.exp(arg) - np.ones(w.shape[0]))
            phi2 += 1.0 / arg ** 2 * (np.exp(arg) - arg - np.ones(w.shape[0]))
            phi3 += 1.0 / arg ** 3 * (np.exp(arg) - 0.5 * arg ** 2 - arg - np.ones(w.shape[0]))
            Q += 2.0 / arg * (np.exp(0.5 * arg) - np.ones(w.shape[0]))

        phi1 = np.real(phi1 / M)
        phi1 = np.matmul(v, np.matmul(np.diag(phi1), inv_v))
        phi2 = np.real(phi2 / M)
        phi2 = np.matmul(v, np.matmul(np.diag(phi2), inv_v))
        phi3 = np.real(phi3 / M)
        phi3 = np.matmul(v, np.matmul(np.diag(phi3), inv_v))
        Q = np.real(Q / M)
        self.Q = np.matmul(v, np.matmul(np.diag(Q), inv_v))

        self.f1 = phi1 - 3 * phi2 + 4 * phi3
        self.f2 = 2 * phi2 - 4 * phi3
        self.f3 = -phi2 + 4 * phi3


    def NonLin(self, a):


        frac2 = np.matmul(self.phi,a) * np.matmul(self.phix,a)

        return -np.matmul(np.transpose(self.phi),frac2)


    def integrate(self,u):

        #assert x.size == 3, 'Input needs 3 entries'

        Nu = self.NonLin(u)
        a = np.matmul(self.E_2, u) + self.h / 2 * np.matmul(self.Q, Nu)
        Na = self.NonLin(a)
        b = np.matmul(self.E_2, u) + self.h / 2 * np.matmul(self.Q, Na)
        Nb = self.NonLin(b)
        c = np.matmul(self.E_2, a) + self.h / 2 * np.matmul(self.Q, 2*Nb-Nu)
        Nc = self.NonLin(c)
        # update rule
        u = np.matmul(self.E,u) + self.h * np.matmul(self.f1, Nu) + self.h * np.matmul(self.f2, Na+Nb) + self.h * np.matmul(self.f3,Nc)

        return u



#setup
epsilon = 0.4
skip_dim = 35
n_runs = 1
plot = True
normalization = False

# Read data from files
np_file = np.load('./KS_data.npz')
X = np_file['X']  # Data
dt = np_file['dt']
t_split = np_file['t_stop_train']
t_skip = np_file['t_skip']
val_ratio = np_file['val_ratio']
Lyap = np_file['Lyap']
coord = np_file['coord']


#ESN setup
K.clear_session()
rng = np.random.RandomState(1)
n_neurons = 500
degree = 3.0  # average number of connections of a unit to other units in reservoir (3.0)
sparseness = 1. - degree / (n_neurons - 1.)  # sparseness of W
input_scaling = 0.4
rho = 0.4
leaking_rate = 1.0
beta = 0.00005



# prepare data for training

# skip beginning
i_skip = int(t_skip / dt)
X = X[i_skip:, :]


#Decomposition
pod = propdec.POD()
eig, a, phi = pod(X,False)

print(eig)
print(eig/np.sum(eig))

#reduce dimension
a_red = a[:,0:-skip_dim]
phi_red = phi[:,0:-skip_dim]

#generate ROM
rom = ROM(phi_red,coord, dt)

#create array with integrated ROM coefficients
#initialize ROM coefficients with first value
a_rom = []
for i in range(a.shape[0]):
    a_rom.append(rom.integrate(a_red[i,:]))

a_rom = np.array(a_rom)

# calculate x_rom
x_rom = np.matmul(a_rom, np.transpose(phi_red))

# Concatenate real solution
input_all = np.concatenate((x_rom, X), axis=1)

# Create mapping
input_all = input_all[:-1, :]
output_all = X[1:, :]

#normalize for better prediction
if normalization == True:
    avg_i = np.average(input_all,axis=0)
    std_i = np.std(input_all, axis=0)
    avg_o = np.average(output_all,axis=0)
    std_o = np.std(output_all, axis=0)

    input_all = (input_all - avg_i) / std_i
    output_all = (output_all - avg_o) / std_o


# split data into training, validation
idx_split = int(t_split / dt) - i_skip
assert idx_split > 0, 'skip time is bigger than split time'
# index that seperates training and validation data
idx_val = int(idx_split * (1 - val_ratio))

input_train = input_all[:idx_split, :]
output_train = output_all[:idx_split, :]


#training_idx = int(training_time/dt)
resync_time = 1
resync_idx = int(resync_time/(Lyap*dt))
washout = 100


#shorten training data
#input_train = input_train[:training_idx,:]
#output_train = output_train[:training_idx,:]

#reshape input
input_train = input_train.reshape((1,input_train.shape[0],input_train.shape[1]))
print(input_train.shape)


#create model with given parameters

model = EchoStateNet.ESN_Hybrid(n_neurons, input_train.shape[2], leaking_rate, rho, sparseness, input_scaling, rng, 'tanh', beta)

#train model
model.fit(input_train, output_train, washout)
model.reset_states()


#reference solution for prediction section
x_ref = X[idx_split:, :]

t_pred = 10 / Lyap
err_t = []

# start prediction
for i in range(n_runs):

    idx_start = int(i * t_pred / dt)
    idx_end = int((i + 1) * t_pred / dt)

    x_init = input_all[idx_split + idx_start - resync_idx:idx_split + idx_start, :]

    assert idx_end < x_ref.shape[0], 't_pred too long'
    x_loc = x_ref[idx_start:idx_end, :]  # local reference solution


    # initialize reservoir
    x_init = x_init.reshape((1, x_init.shape[0], x_init.shape[1]))
    model.predict(x_init)

    x_pred = []

    x_last = input_all[idx_split + idx_start, :]
    x_last = x_last.reshape((1, 1, x_last.shape[0]))

    if normalization == True:
        print('start natural response')
        for i in range(x_loc.shape[0] - 1):
            # predict correction for next timestep
            x_pred.append(model.predict(x_last))

            # compute POD coefficients
            a_last = np.matmul(x_pred[-1] * std_o + avg_o, phi_red)

            # integrate last coefficients
            a_last = rom.integrate(a_last[0, :])

            # ROM state at next time step
            x_new = np.matmul(a_last, np.transpose(phi_red))
            x_new = (x_new - avg_i[0:x_new.shape[0]]) / std_i[0:x_new.shape[0]]


            # Form new input vector
            x_last = np.concatenate((x_new, x_pred[-1][0, :]), axis=0)

            # reshape x_last
            x_last = x_last.reshape(1, 1, x_last.shape[0])

            # print('(%d/%d) predicitions done' % (i, x_loc.shape[0])) if i % 50 == 0 and i != 0 else None

    if normalization == False:

        print('start natural response')
        for i in range(x_loc.shape[0] - 1):
            # predict correction for next timestep
            x_pred.append(model.predict(x_last))

            # compute POD coefficients
            a_last = np.matmul(x_pred[-1], phi_red)

            # integrate last coefficients
            a_last = rom.integrate(a_last[0, :])

            # ROM state at next time step
            x_new = np.matmul(a_last, np.transpose(phi_red))

            # Form new input vector
            x_last = np.concatenate((x_new, x_pred[-1][0, :]), axis=0)

            # reshape x_last
            x_last = x_last.reshape(1, 1, x_last.shape[0])

            # print('(%d/%d) predicitions done' % (i, x_loc.shape[0])) if i % 50 == 0 and i != 0 else None


    x_pred = np.array(x_pred)
    x_pred = x_pred.reshape(x_pred.shape[0], x_pred.shape[2])

    # add first timestep to array
    x_pred = np.vstack((output_all[idx_split + idx_start -1, :], x_pred))

    if normalization == True:
        x_pred = x_pred * std_o + avg_o


    model.reset_states()

    # calculate Error
    err = LA.norm(x_pred - x_loc, axis=1) / np.sqrt(
        np.average(np.square(LA.norm(x_loc, axis=1))))

    t = (np.argmax(err > epsilon) + 1) * dt
    err_t.append(t)

    if plot == True:

        # calculate Error
        err = LA.norm(x_pred - x_loc, axis=1) / np.sqrt(
            np.average(np.square(LA.norm(x_loc, axis=1))))
        plt.plot(np.arange(0, x_loc.shape[0]) * dt * Lyap, err)
        plt.show()
        print((np.argmax(err > epsilon) + 1) * dt * Lyap)

        # plot
        coord, tt = np.meshgrid(coord, np.arange(0, x_loc.shape[0]) * dt * Lyap)

        # plt.contour(tt,x,uu,cmap=cm.coolwarm)
        plot = plt.pcolor(tt, coord, x_pred - x_loc, cmap=cm.coolwarm)
        plt.show()

err_t = np.array(err_t)
print('mean:%f' % np.average(err_t))
print('standarddeviation:%f' % np.std(err_t))




