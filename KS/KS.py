import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.fft import fft, ifft, fftfreq

# KSequ.m - solution of Kuramoto-Sivashinsky equation
#
# u_t = -u*u_x - u_xx - u_xxxx, periodic boundary conditions on [0,length]
# computation is based on v = fft(u), so linear term is diagonal
#
# Using this program:
# u is the initial condition
# h is the time step
# N is the number of points calculated along x (must be an even number)
# x is coordinate vector

save = True
plot = False
POD = False

#data generation
Lyap = 0.07 #for this parameter setting [Pathak, Hybrid forecasting, using ML with knowledge based model]

#training duration in seconds
t_train = 100 / Lyap
#validation ratio
val_ratio = 1.0
#initial transient cut off in seconds
t_skip = 20
#End time of training
t_stop_train = t_train/val_ratio+t_skip
# number of tests with length 10 Lyap times
N = 100
#t_stop is the end time (training + validation + reference for all N simulations)
t_stop = t_stop_train + N * 10 / Lyap

####################
t_plot = 1000
####################

# Initial condition and grid setup
length = 35.0
N = 64
x = length * np.linspace(0, 1, N)
amplitude = 1.0
shift = 0.0
u = np.cos(2*np.pi*x/length)*(1+np.sin(2*np.pi*x/length))
v = fft(u)

# scalars for ETDRK4
h = 0.25
k = fftfreq(N) * N * 2*np.pi/length
L = k**2 - k**4
E = np.exp(h*L)
E_2 = np.exp(h*L/2)
Q = 0
phi1 = 0.0
phi2 = 0.0
phi3 = 0.0


M = 32 #number of evaluation points in Cauchy integral

for j in range(1,M+1):

    arg = h*L + np.ones(L.shape[0]) * np.exp(2j*np.pi*(j-0.5)/M)

    phi1 += 1.0/arg * (np.exp(arg) - np.ones(L.shape[0]))
    phi2 += 1.0/arg**2 * (np.exp(arg) - arg - np.ones(L.shape[0]))
    phi3 += 1.0/arg**3 * (np.exp(arg) - 0.5*arg**2 - arg - np.ones(L.shape[0]))
    Q += 2.0/arg * (np.exp(0.5*arg) - np.ones(L.shape[0]))

phi1 = np.real(phi1/M)
phi2 = np.real(phi2/M)
phi3 = np.real(phi3/M)
Q = np.real(Q/M)

f1 = phi1 - 3*phi2 + 4*phi3 #-4 - L * h + E * (4 - 3 * L * h + (L * h)*(L * h))
f2 = 2*phi2 - 4*phi3 #2 + L * h + E * (-2 + L * h)
f3 = -phi2 + 4*phi3 #-4 - 3 * L * h - (L*h)*(L*h) + E * (4 - L*h)


# main loop
uu = np.array([u])
tt = [0]


for t in np.arange(h,t_stop+h,h):

    Nv = -0.5j*k * fft(np.real(ifft(v))**2)
    a = E_2 * v + h/2 * Q * Nv
    Na = -0.5j*k * fft(np.real(ifft(a))**2)
    b = E_2 * v + h/2 * Q * Na
    Nb = -0.5j*k * fft(np.real(ifft(b))**2)
    c = E_2 * a + h/2 * Q * (2 * Nb - Nv)
    Nc = -0.5j*k * fft(np.real(ifft(c))**2)
    #update rule
    v = E * v + h*f1*Nv + h*f2*(Na+Nb) + h*f3*Nc

    #save data
    u = np.real(ifft(v))
    uu = np.vstack((uu,u))
    tt.append(t)


if save == True:
    np.savez('KS_data', X=uu, dt=h, t_stop_train=t_stop_train, t_skip=t_skip, val_ratio=val_ratio, Lyap=Lyap, coord=x)



if plot == True:

    idx_plot = int(t_plot/h)

    # plot
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    x, tt = np.meshgrid(x, np.array(tt[0:idx_plot]))
    surf = ax.plot_surface(tt, x, uu[0:idx_plot,:], cmap=cm.coolwarm, linewidth=0, antialiased=False)
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()

    #plt.contour(tt,x,uu,cmap=cm.coolwarm)
    plot = plt.pcolor(tt,x,uu[0:idx_plot,:],cmap=cm.coolwarm)
    #plt.colorbar(plot,shrink=0.5, aspect=5)
    plt.show()

if POD == True:

    # import POD
    import importlib.util

    spec1 = importlib.util.spec_from_file_location("POD", "/Users/ml/Documents/Studium/Masterarbeit/Code/pemlm/POD.py")
    propdec = importlib.util.module_from_spec(spec1)
    spec1.loader.exec_module(propdec)

    idx_skip = int(t_skip/h)

    # Decomposition
    pod = propdec.POD()
    eig, a, phi = pod(uu[idx_skip:,:], True)

    plt.plot(np.arange(0, eig.shape[0]),eig)
    plt.show()

    plt.plot(np.arange(0, eig.shape[0]),eig/np.sum(eig))
    plt.show()