import os
import numpy as np
import argparse
from numpy import linalg as LA
import matplotlib.pyplot as plt
class POD:

    def __call__(self, input, normalize):
        """Proper Orthogonal Decomposition

        :param input: 2D array (m x n) m: Number of Timesteps, n: Number of Variables
        :param normalize: Boolean True or False, whether to substract mean from each row
        :return: eig: Eigenvalues of autocorrelation matrix, a: temporal coefficients, phi: POD modes
        """
        if normalize == True:
            self.avg = np.average(input, axis=0)
            input = input - self.avg

        #number of timesteps
        m = input.shape[0]
        C = np.matmul(np.transpose(input),input)/(m-1)

        #solve eigenvalue problem
        eig, phi = LA.eigh(C)

        #Sort Eigenvalues and vectors
        idx = eig.argsort()[::-1]
        eig = eig[idx]
        phi = phi[:, idx]
        if normalize == True:
            self.avg = self.avg[idx]

        #project onto modes for temporal coefficients
        a = np.matmul(input,phi)

        return eig, a, phi

    def denorm(self,input):
        #check whether input has been reduced
        dim_in = input.shape[1]

        return input + self.avg[0:dim_in]



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--path', help='relative path to input file', type=str)
    args = parser.parse_args()

    path = os.getcwd()
    path = path + args.path

    file = np.load(path)
    X = file['X']  # Data
    dt = file['dt']
    t_split = file['t_stop_train']

    # skip beginning
    t_skip = 1
    i_skip = int(t_skip / dt)
    X = X[i_skip:, :]

    eig, a, phi = POD(X,True)

    print("check orthogonality")
    print(np.matmul(np.transpose(phi),phi))

    print("relative contribution of eigenvalues")
    print(eig/np.sum(eig))

    print("size of coefficients")
    print(a.shape)

    print("size of input")
    print(X.shape)

if __name__ == '__main__':
    main()