Branch:

cluster: Branch for ML cluster runs
workstat: Contains final versions of all parameter search and reservoir size runs
develop_LorenzSys: Contains the Kolmogorov flow and is the most recent branch except for final versions of all parameter search and reservoir size files




POD.py contains the class and methods for POD decomposition

ESN Folder contains:

ESN.py .... standard ESN used for pure data driven, hybrid 1 and hybrid 2
ESN_Hybrid.py ... ESN architecture for hybrid method 3

Other ESN files contain former, slightly different versions of the ESN

LorenzSys:
Lorenzsys.py integrates the Lorenz System and stores the data
Attractor_Plot.py plots the Lorenz attractor
Lorenzsys_LSTM.py &
Lorenzsys_LSTM2.py contains prediction with LSTM network

For final ESN/Hybrid forecasting versions please use branch: workstat

CDV Folder:
CDV.py integrates the CDV system and creates the data file

For final CDV ESN/Hybrid versions please use branch: workstat

Platt Folder:
Platt.py integrates the CDV system and creates the data file

For final Platt ESN/Hybrid versions please use branch: workstat

KS Folder:
KS.py generates the data file
KS_ROM.py investigates the ROM alone (stability)
For final KS ESN/Hybrid versions please use branch: workstat

Kolmogorov Folder:
kol2d_odd.py generates the data file
kol_ROM.py not working ROM
kol_ROM2.py working ROM inspired by https://www.sandia.gov/~ikalash/tezaur_data_science_reading_group_may18_2017.pdf
gif.py creates gifs for final presentation