import numpy as np
from numpy import linalg as LA
from matplotlib import pyplot as plt

class LorenzSys():

    sigma = 10.0
    r = 28.0
    b = 8.0/3.0

    x0=0
    y0=1
    z0=0
    x0 = [[x0, y0, z0]]


    def dynamics(self, x):

        #assert x.size == 3, 'Input needs 3 entries'

        dxdt = np.zeros((3,))
        dxdt[0] = self.sigma * ( x[1] - x[0] )
        dxdt[1] = self.r * x[0] - x[1] - x[0] * x[2]
        dxdt[2] = x[0] * x[1] - self.b * x[2]

        return dxdt


    def jacobian(self, x):

        #assert x.size == 3, 'Input needs 3 entries'

        J = np.zeros((3,3))
        J[0,0] = -self.sigma
        J[0,1] = self.sigma
        J[0,2] = 0.0
        J[1,0] = self.r - x[2]
        J[1,1] = -1.0
        J[1,2] = -x[0]
        J[2,0] = x[1]
        J[2,1] = x[0]
        J[2,2] = -self.b

        return J

    def integrate(self,x,dt):

        #assert x.size == 3, 'Input needs 3 entries'

        k1 = self.dynamics(x)
        k2 = self.dynamics(x+dt/2.0*k1)
        k3 = self.dynamics(x+dt/2.0*k2)
        k4 = self.dynamics(x+dt*k3)

        return x + dt / 6.0 * (k1 + 2*k2 + 2*k3 + k4)


    def Lyapunovexp(self, solution, t_start, t_end, dt, interval):

        assert t_end/dt >= interval, 'interval is bigger than number of iterations'

        hypersphere = np.identity(3)
        y = np.zeros((3, 3))
        i = 0
        sum1, sum2, sum3 = 0.0 , 0.0 , 0.0

        while i < t_end/dt:

            k1 = np.matmul(self.jacobian(solution[i,:]),hypersphere)
            inter_sol = self.integrate(solution[i,:],dt/2)
            k2 = np.matmul(self.jacobian(inter_sol), hypersphere + dt / 2.0 * k1)
            k3 = np.matmul(self.jacobian(inter_sol), hypersphere + dt / 2.0 * k2)
            k4 = np.matmul(self.jacobian(solution[i+1, :]), hypersphere + dt * k3)

            z = hypersphere + dt / 6.0 * (k1 + 2 * k2 + 2 * k3 + k4)

            if (i%interval == 0 and i!=0 ) or i == t_end/dt-1:
                # gram_schmidt
                y[:,0] = z[:,0]
                length1 = LA.norm(y[:,0])
                y[:,1] = z[:,1] - np.dot(z[:,1], y[:,0] / length1) * y[:,0] / length1
                length2 = LA.norm(y[:,1])
                y[:,2] = z[:,2] - np.dot(z[:,2], y[:,0] / length1) * y[:,0] / length1 \
                          - np.dot(z[:,2], y[:,1] / length2) * y[:,1] / length2
                length3 = LA.norm(y[:, 2])

                print('gram schmidt done at i= %d' %(i))

                # normalize Base and make new hypersphere
                hypersphere[:, 0] = y[:, 0] / length1
                hypersphere[:, 1] = y[:, 1] / length2
                hypersphere[:, 2] = y[:, 2] / length3

                sum1 += np.log(length1)
                sum2 += np.log(length2)
                sum3 += np.log(length3)

            else:
                hypersphere = z

            i+=1

        # ref Values: Lyapunov exponents in constrained and unconstrained ordinary differential equations
        lambda1 = sum1 / t_end
        lambda2 = sum2 / t_end
        lambda3 = sum3 / t_end

        return np.array([lambda1, lambda2, lambda3])

    def Lyapunovexp_ext(self, solution, t_start, t_end, dt, interval):

        assert t_end/dt >= interval, 'interval is bigger than number of iterations'
        lambda1 = []
        lambda2 = []
        lambda3 = []
        iterate = 0

        for angle in np.linspace(0,2*np.pi,50):
            print('calculate sphere %d '% iterate)
            iterate += 1

            S = np.array([[1, 0, 0], [0, np.cos(angle), -np.sin(angle)], [0, np.sin(angle), np.cos(angle)]])
            T = np.array([[np.cos(angle), -np.sin(angle), 0], [np.sin(angle), np.cos(angle), 0], [0, 0, 1]])
            hypersphere = np.matmul(T, np.matmul(S, np.identity(3)))
            y = np.zeros((3, 3))
            i = 0
            sum1, sum2, sum3 = 0.0, 0.0, 0.0

            while i < t_end / dt:

                k1 = np.matmul(self.jacobian(solution[i, :]), hypersphere)
                inter_sol = self.integrate(solution[i, :], dt / 2)
                k2 = np.matmul(self.jacobian(inter_sol), hypersphere + dt / 2.0 * k1)
                k3 = np.matmul(self.jacobian(inter_sol), hypersphere + dt / 2.0 * k2)
                k4 = np.matmul(self.jacobian(solution[i + 1, :]), hypersphere + dt * k3)

                z = hypersphere + dt / 6.0 * (k1 + 2 * k2 + 2 * k3 + k4)

                if (i % interval == 0 and i != 0) or i == t_end / dt - 1:
                    # gram_schmidt
                    y[:, 0] = z[:, 0]
                    length1 = LA.norm(y[:, 0])
                    y[:, 1] = z[:, 1] - np.dot(z[:, 1], y[:, 0] / length1) * y[:, 0] / length1
                    length2 = LA.norm(y[:, 1])
                    y[:, 2] = z[:, 2] - np.dot(z[:, 2], y[:, 0] / length1) * y[:, 0] / length1 \
                              - np.dot(z[:, 2], y[:, 1] / length2) * y[:, 1] / length2
                    length3 = LA.norm(y[:, 2])

                    #print('gram schmidt done at i= %d' % (i))

                    # normalize Base and make new hypersphere
                    hypersphere[:, 0] = y[:, 0] / length1
                    hypersphere[:, 1] = y[:, 1] / length2
                    hypersphere[:, 2] = y[:, 2] / length3

                    sum1 += np.log(length1)
                    sum2 += np.log(length2)
                    sum3 += np.log(length3)

                else:
                    hypersphere = z

                i += 1

            # ref Values: Lyapunov exponents in constrained and unconstrained ordinary differential equations
            lambda1.append(sum1 / t_end)
            lambda2.append(sum2 / t_end)
            lambda3.append(sum3 / t_end)

        lambda1 = np.sum(lambda1)/lambda1.__len__()
        lambda2 = np.sum(lambda2)/lambda2.__len__()
        lambda3 = np.sum(lambda3)/lambda3.__len__()

        return np.array([lambda1, lambda2, lambda3])


def main():

    save = 1
    plot = 0
    Lyapunov_calc = 0

    LSys = LorenzSys()

    dt = 0.01
    Lyap = 1.0  # biggest positive Lyapunov exponent
    t_start = 0
    t_train = 100/Lyap
    t_skip = 20
    val_ratio = 0.8
    t_stop_train = t_train/val_ratio+t_skip #assuming val=0.8 and tskip=1
    N = 100 #number of tests
    #t_stop is the end time (training + validation + reference for all N simulations)
    t_stop = t_stop_train + N * 10 / Lyap #10 is approx 10 Lyap times

    X = LSys.x0

#integration
    print('start integration loop')
    for t , val in np.ndenumerate(np.arange(t_start,t_stop,dt)):
        X.append(LSys.integrate(X[t[0]],dt))
        print('calculated time=%.3f of %.3f'%(t[0]*dt,t_stop))

    X = np.array(X)
    print(X.shape)



    if Lyapunov_calc == 1:
        print('start Lyapunov calc')
        lambdas = LSys.Lyapunovexp(X, t_start, t_stop, dt, 1) #50 Unit discs
        print(lambdas)
        print(np.sum(lambdas))

    # save data
    if save == 1:
       np.savez('Lorenz_data',X=X,dt=dt,t_stop_train=t_stop_train,t_skip=t_skip, val_ratio=val_ratio,Lyap=Lyap)

    ## plot trajectory in (x1, x4) plane
    if plot == 1:
        plt.plot(X[:,0], X[:,1], linewidth=0.1)
        #plt.savefig('Lorenz_attractor.pdf',format='pdf')
        plt.show()

        plt.plot(X[2000:8000,0])
        plt.show()

        plt.plot(X[2000:8000,1])
        plt.show()

        plt.plot(X[2000:8000,2])
        plt.show()


if __name__ == '__main__':
    main()