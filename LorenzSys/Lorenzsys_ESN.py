import os
import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as LA
import matplotlib as mpl
#import ESN
import importlib.util
spec = importlib.util.spec_from_file_location("ESN", "/Users/ml/Documents/Studium/Masterarbeit/Code/pemlm/ESN/ESN.py")
EchoStateNet = importlib.util.module_from_spec(spec)
spec.loader.exec_module(EchoStateNet)

n_neurons = 500
rng = np.random.RandomState(1)
degree = 4.11  # average number of connections of a unit to other units in reservoir (3.0)
sparseness = 1. - degree / (n_neurons - 1.)  # sparseness of W

model = EchoStateNet.ESN(n_neurons, 3, 1.0, 0.58, sparseness, 0.6, rng, 'tanh',0.00001)


epsilon = 0.2
normalization = 0
# Read data from files
os.chdir("/Users/ml/Documents/Studium/Masterarbeit/Code/pemlm/LorenzSys/")
np_file = np.load('./Lorenz_data.npz')
X = np_file['X']  # Data
dt = np_file['dt']
t_split = np_file['t_stop_train']
t_skip = np_file['t_skip']
val_ratio = np_file['val_ratio']
Lyap = np_file['Lyap']

# prepare data for training

# skip beginning
i_skip = int(t_skip / dt)
X = X[i_skip:, :]

# normalize data
stddev = np.std(X, axis=0)
avg = np.average(X, axis=0)

if normalization == 0:
    X = (X - avg) / stddev

if normalization == 1:
    X = X - avg
    maxi = np.array([np.amax(np.absolute(X[:, 0])), np.amax(np.absolute(X[:, 1])), np.amax(np.absolute(X[:, 2]))])
    X = X / maxi

input_all = X[:-1, :]
output_all = X[1:, :]

# split data into training, validation
idx_split = int(t_split / dt) - i_skip
assert idx_split > 0, 'skip time is bigger than split time'
# index that seperates training and validation data
idx_val = int(idx_split * (1 - val_ratio))

input_train = input_all[:idx_split, :]
output_train = output_all[:idx_split, :]

#training_time = 10
#training_idx = int(training_time/dt)
resync_time = 1
resync_idx = int(resync_time/dt)
washout = 100

#shorten training data
#input_train = input_train[:training_idx,:]
#output_train = output_train[:training_idx,:]

#reshape input
input_train = input_train.reshape((1,input_train.shape[0],input_train.shape[1]))
print(input_train.shape)

model.fit(input_train,output_train,washout)
model.reset_states()

#Teacher forced prediction
#Y = model.predict(input_train[:,washout:,:])


#plt.plot(input_train[0, washout:, 0])
#plt.plot(Y[:, 0], '--')
#plt.show()

#plt.plot(input_train[0, washout:, 1])
#plt.plot(Y[:, 1], '--')
#plt.show()

#plt.plot(input_train[0, washout:, 2])
#plt.plot(Y[:, 2], '--')
#plt.show()

#model.reset_states()



t_pred = 10 / Lyap
idx_end = int(t_pred / dt)
x_init = X[idx_split-resync_idx:idx_split,:]
x_ref = X[idx_split:, :]
assert idx_end < x_ref.shape[0], 't_pred too long'
x_ref = x_ref[:idx_end, :]

#initialize reservoir
x_init = x_init.reshape((1,x_init.shape[0],x_init.shape[1]))
model.predict(x_init)

Y = []

y_last = x_ref[0,:].reshape((1, 1, x_ref.shape[1]))
Y.append(x_ref[0,:].reshape((1,x_ref.shape[1])))

print('start natural response')
for i in range(x_ref.shape[0] - 1):
    Y.append(model.predict(y_last))
    y_last = Y[-1].reshape((1, 1, x_ref.shape[1]))
    print('[%d/%d] predicitions done' % ((i),x_ref.shape[0])) if i%100 == 0 else None

Y = np.array(Y)
Y = Y.reshape(Y.shape[0], Y.shape[2])


# denormalize
if normalization == 0:
    Y = Y * stddev + avg
    x_ref = x_ref * stddev + avg

if normalization == 1:
    Y = Y * maxi + avg
    x_ref = x_ref * maxi + avg


mpl.rcParams['legend.fontsize'] = 11
#mpl.use("pgf")
mpl.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
    'figure.autolayout': True,
})


t = range(x_ref.shape[0])*dt*0.9

# calculate Error
err = LA.norm(Y[:, :] - x_ref[:, :], axis=1) / np.sqrt(
    np.average(np.square(LA.norm(x_ref[:, :], axis=1))))
t_val = (np.argmax(err > epsilon) + 1) * dt * 0.9
print("Valid time: %f"% t_val)


plt.plot(t,err)
plt.xlabel("$\lambda\cdot t$")
plt.ylabel("$E(t)$",rotation='horizontal')
plt.axvline(t[np.argmax(err > epsilon)],0,1,label='valid time',color='red')
plt.legend()
plt.savefig("/Users/ml/Documents/Studium/Masterarbeit/Thesis/TFD_template/media/Lorenz_ESN_err_single.pgf")
#plt.show()

plt.clf()


ax1 = plt.subplot(311)
plt.plot(t,x_ref[:, 0],label='Reference')
plt.plot(t,Y[:, 0], '--',label='Prediction')
plt.axvline(t[np.argmax(err > epsilon)],0,1,label='valid time',color='red')
plt.legend()
plt.setp(ax1.get_xticklabels(), visible=False)
ax1.set_ylabel('$X_1$',rotation=0)

# share x only
ax2 = plt.subplot(312, sharex=ax1)
plt.plot(t,x_ref[:, 1])
plt.plot(t,Y[:, 1], '--')
plt.axvline(t[np.argmax(err > epsilon)],0,1,color='red')
# make these tick labels invisible
plt.setp(ax2.get_xticklabels(), visible=False)
ax2.set_ylabel('$X_2$',rotation=0)

# share x
ax3 = plt.subplot(313, sharex=ax2)
plt.plot(t,x_ref[:, 2])
plt.plot(t,Y[:, 2], '--')
plt.axvline(t[np.argmax(err > epsilon)],0,1,color='red')
ax3.set_xlabel('$\lambda\cdot t$',fontsize=11)
ax3.set_ylabel('$X_3$',rotation=0) #labelpad
#plt.xlim(0.01, 5.0)
#plt.show()
plt.savefig("/Users/ml/Documents/Studium/Masterarbeit/Thesis/TFD_template/media/Lorenz_ESN_x_single.pgf")


plt.clf()

ax1 = plt.subplot(411)
ax1.plot(t,x_ref[:, 0],label='Reference',linewidth=1)
ax1.plot(t,Y[:, 0], '--',label='Prediction',linewidth=1)
plt.axvline(t[np.argmax(err > epsilon)],0,1,label='valid time',color='red')
ax1.legend(loc='upper left')
plt.setp(ax1.get_xticklabels(), visible=False)
ax1.set_ylabel('$X_1$',rotation=0)

ax2 = plt.subplot(412, sharex=ax1)
ax2.plot(t,x_ref[:, 1],linewidth=1)
ax2.plot(t,Y[:, 1], '--' ,linewidth=1)
plt.setp(ax2.get_xticklabels(), visible=False)
plt.axvline(t[np.argmax(err > epsilon)],0,1,color='red')
ax2.set_ylabel('$X_2$',rotation=0)

ax3 = plt.subplot(413, sharex=ax2)
ax3.plot(t,x_ref[:, 2], linewidth=1)
ax3.plot(t,Y[:, 2], '--',linewidth=1)
plt.setp(ax3.get_xticklabels(), visible=False)
plt.axvline(t[np.argmax(err > epsilon)],0,1,color='red')
ax3.set_ylabel('$X_3$',rotation=0)

ax4 = plt.subplot(414, sharex=ax3)
ax4.plot(t,err,label='$E(t)$',linewidth=1)
ax4.set_ylabel('$E(t)$',rotation=0)
ax4.legend(loc='upper left')
plt.axvline(t[np.argmax(err > epsilon)],0,1,color='red')
ax4.set_xlabel('$t\cdot \lambda$',fontsize=11)

plt.gcf().align_ylabels([ax1,ax2,ax3,ax4])
#plt.gcf().set_figheight(8, forward=True)
plt.gcf().set_size_inches(15*0.3937, 15*0.3937, forward=True)

#plt.show()
plt.savefig("/Users/ml/Documents/Studium/Masterarbeit/Thesis/TFD_template/media/Lorenz_ESN_single_comb.pgf",dpi=plt.gcf().dpi)
