import os
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import tikzplotlib

os.chdir("/Users/ml/Documents/Studium/Masterarbeit/Code/pemlm/LorenzSys/")
np_file = np.load('./Lorenz_data.npz')
X = np_file['X']  # Data
dt = np_file['dt']
t_split = np_file['t_stop_train']
t_skip = np_file['t_skip']
val_ratio = np_file['val_ratio']
Lyap = np_file['Lyap']

# skip beginning
i_skip = int(t_skip / dt)
X = X[i_skip:, :]

t = range(X.shape[0])*dt

#t = np.arange(0.01, 5.0, 0.01)
#s1 = np.sin(2 * np.pi * t)
#s2 = np.exp(-t)
#s3 = np.sin(4 * np.pi * t)

mpl.rcParams['legend.fontsize'] = 10
#mpl.use("pgf")
mpl.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})


ax1 = plt.subplot(311)
plt.plot(t[:10000],X[:10000,0],linewidth=0.5)
plt.setp(ax1.get_xticklabels(), visible=False)
ax1.set_ylabel('$X_1$',rotation=0)

# share x only
ax2 = plt.subplot(312, sharex=ax1)
plt.plot(t[:10000],X[:10000,1], linewidth=0.5)
# make these tick labels invisible
plt.setp(ax2.get_xticklabels(), visible=False)
ax2.set_ylabel('$X_2$',rotation=0)

# share x
ax3 = plt.subplot(313, sharex=ax2)
plt.plot(t[:10000],X[:10000,2], linewidth=0.5)
ax3.set_xlabel('t in [s]',fontsize=11)
ax3.set_ylabel('$X_3$',rotation=0) #labelpad
#plt.xlim(0.01, 5.0)
plt.show()

#plt.savefig("/Users/ml/Documents/Studium/Masterarbeit/Thesis/TFD_template/media/Lorenz_time_evol.pgf")

plt.clf()

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot(X[:10000,0],X[:10000,1],X[:10000,2], label='Lorenz Attractor',linewidth=0.2)
ax.set_xlabel('$X_1$',rotation='horizontal')
ax.set_ylabel('$X_2$',rotation='horizontal')
ax.set_zlabel('$X_3$',rotation='horizontal')

# make the panes transparent
ax.xaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
ax.yaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
ax.zaxis.set_pane_color((1.0, 1.0, 1.0, 0.0))
# make the grid lines transparent
#ax.xaxis._axinfo["grid"]['color'] =  (1,1,1,0)
#ax.yaxis._axinfo["grid"]['color'] =  (1,1,1,0)
#ax.zaxis._axinfo["grid"]['color'] =  (1,1,1,0)

ax.legend()

plt.show()
#tikzplotlib.save("/Users/ml/Documents/Studium/Masterarbeit/Thesis/TFD_template/media/test.tex")
#plt.savefig("/Users/ml/Documents/Studium/Masterarbeit/Thesis/TFD_template/media/Attractor.pgf")