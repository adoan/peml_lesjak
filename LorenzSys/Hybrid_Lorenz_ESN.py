import os
import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as LA

#import POD
import importlib.util
spec1 = importlib.util.spec_from_file_location("POD", "/Users/ml/Documents/Studium/Masterarbeit/Code/pemlm/POD.py")
propdec = importlib.util.module_from_spec(spec1)
spec1.loader.exec_module(propdec)

#import ESN
spec2 = importlib.util.spec_from_file_location("ESN", "/Users/ml/Documents/Studium/Masterarbeit/Code/pemlm/ESN/ESN.py")
EchoStateNet = importlib.util.module_from_spec(spec2)
spec2.loader.exec_module(EchoStateNet)


skip_dim = 1


class ROM():

    sigma = 10.0
    r = 28.0
    b = 8.0/3.0


    def dynamics(self, a, phi):

        #x = a * transpose(phi)

        x = np.matmul(a,np.transpose(phi))

        dxdt = np.zeros((3,))
        dxdt[0] = self.sigma * ( x[1] - x[0] )
        dxdt[1] = self.r * x[0] - x[1] - x[0] * x[2]
        dxdt[2] = x[0] * x[1] - self.b * x[2]

        dadt = np.matmul(dxdt,phi)

        return dadt


    def integrate(self,a,phi,dt):

        #assert x.size == 3, 'Input needs 3 entries'

        k1 = self.dynamics(a,phi)
        k2 = self.dynamics(a+dt/2.0*k1,phi)
        k3 = self.dynamics(a+dt/2.0*k2,phi)
        k4 = self.dynamics(a+dt*k3,phi)

        return a + dt / 6.0 * (k1 + 2*k2 + 2*k3 + k4)


#setup
epsilon = 0.4
normalization = 1
# Read data from files
os.chdir("/Users/ml/Documents/Studium/Masterarbeit/Code/pemlm/LorenzSys/")
np_file = np.load('./Lorenz_data.npz')
X = np_file['X']  # Data
dt = np_file['dt']
t_split = np_file['t_stop_train']
t_skip = np_file['t_skip']
val_ratio = np_file['val_ratio']
Lyap = np_file['Lyap']



# prepare data for training

# skip beginning
i_skip = int(t_skip / dt)
X = X[i_skip:, :]


#Decomposition
pod = propdec.POD()
eig, a, phi = pod(X, False)
print(eig)
print(eig/np.sum(eig))

#generate ROM
rom = ROM()

#reduce dimension
a_red = a[:,0:-skip_dim]
phi_red = phi[:,0:-skip_dim]

#create array with integrated ROM coefficients
#initialize ROM coefficients with first value
a_rom = [a_red[0,:]]
for i in range(a.shape[0]-1):
    a_rom.append(rom.integrate(a_red[i,:],phi_red,dt))

a_rom = np.array(a_rom)

#calculate error term
C = a_red - a_rom

#mapping a_red(true at t) --> C(at t+1)
input_all = a_red[:-1,:]
output_all = C[1:,:]


#normalize for better prediction
if normalization == 1:
    avg_i = np.average(input_all,axis=0)
    std_i = np.std(input_all, axis=0)
    avg_o = np.average(output_all,axis=0)
    std_o = np.std(output_all, axis=0)

    input_all = (input_all - avg_i) / std_i
    output_all = (output_all - avg_o) / std_o

if normalization == 2:
    avg_i = np.average(input_all,axis=0)
    std_i = np.max(np.abs(input_all),0)
    avg_o = np.average(output_all,axis=0)
    std_o = np.max(np.abs(output_all),0)

    input_all = (input_all - avg_i) / std_i
    output_all = (output_all - avg_o) / std_o

if normalization == 3:
    avg_i = np.average(input_all,axis=0)
    std_i = np.ones(input_all.shape[1])
    avg_o = np.average(output_all,axis=0)
    std_o = np.ones(output_all.shape[1])

    input_all = (input_all - avg_i) / std_i
    output_all = (output_all - avg_o) / std_o


# split data into training, validation
idx_split = int(t_split / dt) - i_skip
assert idx_split > 0, 'skip time is bigger than split time'
# index that seperates training and validation data
idx_val = int(idx_split * (1 - val_ratio))

input_train = input_all[:idx_split, :]
output_train = output_all[:idx_split, :]


#training_idx = int(training_time/dt)
resync_time = 1
resync_idx = int(resync_time/dt)
washout = 100


#reshape input
input_train = input_train.reshape((1,input_train.shape[0],input_train.shape[1]))
print(input_train.shape)


#Create ESN with params
n_neurons = 500
rng = np.random.RandomState(1)
degree = 4.11  # average number of connections of a unit to other units in reservoir (3.0)
input_scaling = 0.9875
rho = 0.405
leaking_rate = 1.0
beta = 0.000067

#create model with given parameters
sparseness = 1. - degree / (n_neurons - 1.)  # sparseness of W

model = EchoStateNet.ESN(n_neurons, input_train.shape[2], leaking_rate, rho, sparseness, input_scaling, rng, 'tanh', beta)

# train model
model.fit(input_train, output_train, washout)
model.reset_states()



#Predicition part
t_pred = 10 / Lyap
idx_end = int(t_pred / dt)
a_init = input_all[idx_split-resync_idx:idx_split,:]
x_ref = X[idx_split:, :]
assert idx_end < x_ref.shape[0], 't_pred too long'
x_ref = x_ref[:idx_end, :]

#initialize reservoir
a_init = a_init.reshape((1,a_init.shape[0],a_init.shape[1]))
model.predict(a_init)

C = []
a_pred = []

a_last = input_all[idx_split,:]
a_last = a_last.reshape((1, 1, a_last.shape[0]))

if normalization == True:

    print('start natural response')
    for i in range(x_ref.shape[0] - 1):
        # predict correction for next timestep
        C.append(model.predict(a_last))

        # integrate last coefficients
        a_int = rom.integrate(a_last[0, -1, :] * std_i + avg_i, phi_red, dt)

        # correct ROM model with predicted value
        a_pred.append(a_int + C[-1] * std_o + avg_o)

        a_last = (a_pred[-1] - avg_i) / std_i

        # append last predicition to end of input
        a_last = a_last.reshape(1, a_pred[-1].shape[0], a_pred[-1].shape[1])

        # print('(%d/%d) predicitions done' % (i, x_loc.shape[0])) if i % 50 == 0 and i != 0 else None

if normalization == False:

    print('start natural response')
    for i in range(x_ref.shape[0] - 1):
        # predict correction for next timestep
        C.append(model.predict(a_last))

        # integrate last coefficients
        a_int = rom.integrate(a_last[0, -1, :], phi_red, dt)

        # correct ROM model with predicted value
        a_pred.append(a_int + C[-1])  # * std_o + avg_o

        a_last = a_pred[-1]  # (- avg_i)/std_i

        # append last predicition to end of input
        a_last = a_last.reshape(1, a_pred[-1].shape[0], a_pred[-1].shape[1])

        # print('(%d/%d) predicitions done' % (i, x_loc.shape[0])) if i % 50 == 0 and i != 0 else None

a_pred = np.array(a_pred)
a_pred = a_pred.reshape(a_pred.shape[0], a_pred.shape[2])

#add first timestep to array
a_pred = np.vstack((a_red[idx_split,:], a_pred))

# backtransformation
x_pred = np.matmul(a_pred, np.transpose(phi_red))


# calculate Error
err = LA.norm(x_pred - x_ref, axis=1) / np.sqrt(
    np.average(np.square(LA.norm(x_ref, axis=1))))
plt.plot(err)
plt.show()

plt.plot(x_ref[:, 0])
plt.plot(x_pred[:, 0], '--')
plt.show()

plt.plot(x_ref[:, 1])
plt.plot(x_pred[:, 1], '--')
plt.show()

plt.plot(x_ref[:, 2])
plt.plot(x_pred[:, 2], '--')
plt.show()

