import numpy as np
from numpy import linalg as LA
from matplotlib import pyplot as plt
import scipy
from scipy.linalg import norm
import scipy.stats
import h5py

class CDV_Sys():


    param = [0.95, -0.76095, 0.1, 1.25, 0.2, 0.5]

    x1 = 1.0
    x2 = 1.0
    x3 = 1.0
    x4 = 1.0
    x5 = 1.0
    x6 = 1.0
    x0 = [[x1, x2, x3, x4, x5, x6]]

    def alpha(self,m):

        return (8*np.sqrt(2)*m*m*(self.param[5]*self.param[5]+m*m-1))/(np.pi*(4*m*m-1)*(self.param[5]*self.param[5]+m*m))

    def beta(self,m):

        return (self.param[3]*self.param[5]*self.param[5])/(self.param[5]*self.param[5]+m*m)

    def delta(self,m):

        return (64*np.sqrt(2)*(self.param[5]*self.param[5]-m*m+1))/(15*np.pi*(self.param[5]*self.param[5]+m*m))

    def gamma_star(self,m):

        return self.param[4]*(4*np.sqrt(2)*m*self.param[5])/(np.pi*(4*m*m-1))

    def epsilon(self):

        return 16*np.sqrt(2)/(5*np.pi)

    def gamma_m(self,m):

        return self.param[4]*(4*np.sqrt(2)*m*m*m*self.param[5])/(np.pi*(4*m*m-1)*(m*m+self.param[5]*self.param[5]))


    def dynamics(self, x):

        #assert x.size == 3, 'Input needs 3 entries'

        dxdt = np.zeros((6,))
        dxdt[0] = self.gamma_star(1)*x[2] - self.param[2]*(x[0]-self.param[0])
        dxdt[1] = -(self.alpha(1)*x[0]-self.beta(1))*x[2] - self.param[2]*x[1] - self.delta(1)*x[3]*x[5]
        dxdt[2] = (self.alpha(1)*x[0]-self.beta(1))*x[1] - self.gamma_m(1)*x[0] - self.param[2]*x[2] + self.delta(1)*x[3]*x[4]
        dxdt[3] = self.gamma_star(2)*x[5] - self.param[2]*(x[3]-self.param[1]) + self.epsilon()*(x[1]*x[5]-x[2]*x[4])
        dxdt[4] = -(self.alpha(2)*x[0]-self.beta(2))*x[5] - self.param[2]*x[4] - self.delta(2)*x[3]*x[2]
        dxdt[5] = (self.alpha(2)*x[0]-self.beta(2))*x[4] - self.gamma_m(2)*x[3] - self.param[2]*x[5] + self.delta(2)*x[3]*x[1]

        return dxdt


    def jacobian(self, x):

        #assert x.size == 3, 'Input needs 3 entries'

        J = np.zeros((6,6))
        J[0,0] = -self.param[2]
        J[0,1] = 0.0
        J[0,2] = self.gamma_star(1)
        J[0,3] = 0.0
        J[0,4] = 0.0
        J[0,5] = 0.0
        J[1,0] = -self.alpha(1)*x[2]
        J[1,1] = -self.param[2]
        J[1,2] = -self.alpha(1)*x[0] + self.beta(1)
        J[1,3] = -self.delta(1)*x[5]
        J[1,4] = 0.0
        J[1,5] = -self.delta(1)*x[3]
        J[2,0] = self.alpha(1)*x[1] - self.gamma_m(1)
        J[2,1] = self.alpha(1)*x[0] - self.beta(1)
        J[2,2] = -self.param[2]
        J[2,3] = self.delta(1)*x[4]
        J[2,4] = self.delta(1)*x[3]
        J[2,5] = 0.0
        J[3,0] = 0.0
        J[3,1] = self.epsilon()*x[5]
        J[3,2] = -self.epsilon()*x[4]
        J[3,3] = -self.param[2]
        J[3,4] = -self.epsilon()*x[2]
        J[3,5] = self.gamma_star(2)+self.epsilon()*x[1]
        J[4,0] = -self.alpha(2)*x[5]
        J[4,1] = 0.0
        J[4,2] = -self.delta(2)*x[3]
        J[4,3] = -self.delta(2)*x[2]
        J[4,4] = -self.param[2]
        J[4,5] = self.beta(2)-self.alpha(2)*x[0]
        J[5,0] = self.alpha(2)*x[4]
        J[5,1] = self.delta(2)*x[3]
        J[5,2] = 0.0
        J[5,3] = -self.gamma_m(2)+self.delta(2)*x[1]
        J[5,4] = self.alpha(2)*x[0]-self.beta(2)
        J[5,5] = -self.param[2]

        return J

    def integrate(self,x,dt):

        #assert x.size == 3, 'Input needs 3 entries'

        k1 = self.dynamics(x)
        k2 = self.dynamics(x+dt/2.0*k1)
        k3 = self.dynamics(x+dt/2.0*k2)
        k4 = self.dynamics(x+dt*k3)

        return x + dt / 6.0 * (k1 + 2*k2 + 2*k3 + k4)


    def Lyapunovexp(self, solution, t_start, t_end, dt, interval):

        assert t_end/dt >= interval, 'interval is bigger than number of iterations'

        hypersphere = np.identity(6)
        y = np.zeros((6, 6))
        i = 0
        sum1, sum2, sum3 ,sum4, sum5, sum6 = 0.0 , 0.0 , 0.0, 0.0, 0.0, 0.0

        while i < t_end/dt:

            k1 = np.matmul(self.jacobian(solution[i,:]),hypersphere)
            inter_sol = self.integrate(solution[i,:],dt/2)
            k2 = np.matmul(self.jacobian(inter_sol), hypersphere + dt / 2.0 * k1)
            k3 = np.matmul(self.jacobian(inter_sol), hypersphere + dt / 2.0 * k2)
            k4 = np.matmul(self.jacobian(solution[i+1, :]), hypersphere + dt * k3)

            z = hypersphere + dt / 6.0 * (k1 + 2 * k2 + 2 * k3 + k4)

            if (i%interval == 0 and i!=0 ) or i == t_end/dt-1:
                # gram_schmidt
                y[:,0] = z[:,0]
                length1 = LA.norm(y[:,0])
                y[:,1] = z[:,1] - np.dot(z[:,1], y[:,0] / length1) * y[:,0] / length1
                length2 = LA.norm(y[:,1])
                y[:,2] = z[:,2] - np.dot(z[:,2], y[:,0] / length1) * y[:,0] / length1 \
                          - np.dot(z[:,2], y[:,1] / length2) * y[:,1] / length2
                length3 = LA.norm(y[:, 2])
                y[:,3] = z[:,3] - np.dot(z[:,3], y[:,0] / length1) * y[:,0] / length1 \
                          - np.dot(z[:,3], y[:,1] / length2) * y[:,1] / length2 \
                          - np.dot(z[:,3], y[:,2] / length3) * y[:,2] / length3
                length4 = LA.norm(y[:,3])
                y[:,4] = z[:,4] - np.dot(z[:,4], y[:,0] / length1) * y[:,0] / length1 \
                          - np.dot(z[:,4], y[:,1] / length2) * y[:,1] / length2 \
                          - np.dot(z[:,4], y[:,2] / length3) * y[:,2] / length3 \
                          - np.dot(z[:,4], y[:,3] / length4) * y[:,3] / length4
                length5 = LA.norm(y[:,4])
                y[:,5] = z[:,5] - np.dot(z[:,5], y[:,0] / length1) * y[:,0] / length1 \
                          - np.dot(z[:,5], y[:,1] / length2) * y[:,1] / length2 \
                          - np.dot(z[:,5], y[:,2] / length3) * y[:,2] / length3 \
                          - np.dot(z[:,5], y[:,3] / length4) * y[:,3] / length4 \
                          - np.dot(z[:,5], y[:,4] / length5) * y[:,4] / length5
                length6 = LA.norm(y[:,5])

                print('gram schmidt done at i= %d' %(i))

                # normalize Base and make new hypersphere
                hypersphere[:, 0] = y[:, 0] / length1
                hypersphere[:, 1] = y[:, 1] / length2
                hypersphere[:, 2] = y[:, 2] / length3
                hypersphere[:, 3] = y[:, 3] / length4
                hypersphere[:, 4] = y[:, 4] / length5
                hypersphere[:, 5] = y[:, 5] / length6

                sum1 += np.log(length1)
                sum2 += np.log(length2)
                sum3 += np.log(length3)
                sum4 += np.log(length4)
                sum5 += np.log(length5)
                sum6 += np.log(length6)

            else:
                hypersphere = z

            i+=1

        # ref Values: Lyapunov exponents in constrained and unconstrained ordinary differential equations
        lambda1 = sum1 / t_end
        lambda2 = sum2 / t_end
        lambda3 = sum3 / t_end
        lambda4 = sum4 / t_end
        lambda5 = sum5 / t_end
        lambda6 = sum6 / t_end

        return np.array([lambda1, lambda2, lambda3, lambda4, lambda5, lambda6])


save = 0
plot = 0
Lyapunov_calc = 0

CDV = CDV_Sys()

dt = 0.1
Lyap = 0.02  # biggest positive Lyapunov exponent
t_start = 0
t_train = 100/Lyap #training data = 100 Lyap times
t_skip = 500
val_ratio = 0.8
t_stop_train = t_skip  # assuming val=0.8 and tskip=1
N = 100  # number of tests
# t_stop is the end time (training + validation + reference for all N simulations)
t_stop = t_stop_train 
print('t_stop=%f'%t_stop)

Y_all = []

X = CDV.x0

#integration initial transient
print('start integration loop')
for t , val in np.ndenumerate(np.arange(t_start,t_stop,dt)):
    X.append(CDV.integrate(X[t[0]],dt))

t_stop = 30/Lyap
Y = [X[-1]]
for t , val in np.ndenumerate(np.arange(t_start,t_stop,dt)):
    Y.append(CDV.integrate(Y[t[0]],dt))

Y = np.array(Y)
Y_all.append(Y)

Y = [X[-1]]
Y[0][0] = Y[0][0] + 1e-6
for t , val in np.ndenumerate(np.arange(t_start,t_stop,dt)):
    Y.append(CDV.integrate(Y[t[0]],dt))

Y = np.array(Y)
Y_all.append(Y)


# Calculate Error
error = norm(np.absolute(np.array(Y_all[0])-np.array(Y_all[1])),axis=1)

hf = h5py.File('CDV_Lyap_deviation_traj.h5','w')
hf.create_dataset('error',data=error)
hf.close()

# show error
t = np.linspace(0,(len(error)-1)*dt,len(error))
plt.plot(t,np.log10(error),'k',linewidth=1)
plt.show()

#Curve fitting
ti = int(30/dt)
tf = int(187/dt)
P = np.polyfit(t[ti:tf],np.log(error[ti:tf]),1)
f = np.poly1d(P)
tFit = np.arange(ti*dt,tf*dt,dt)
errorFit = f(tFit)
slope, R2, _, _, _ = scipy.stats.linregress(t[ti:tf],np.log(error[ti:tf]))
print('Maximal Lyapunov Exponent: %f' % P[0])
# approximately 0.022-0.023, so Lyapunov time of approx 41-45

plt.plot(t[ti:tf],np.log10(error[ti:tf]),'k',linewidth=1)
plt.plot(tFit,errorFit*np.log10(np.e),'k--')
plt.show()


