import os
import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as LA

#import POD
import importlib.util
spec1 = importlib.util.spec_from_file_location("POD", "/Users/ml/Documents/Studium/Masterarbeit/Code/pemlm/POD.py")
propdec = importlib.util.module_from_spec(spec1)
spec1.loader.exec_module(propdec)

#import ESN
spec2 = importlib.util.spec_from_file_location("ESN", "/Users/ml/Documents/Studium/Masterarbeit/Code/pemlm/ESN/ESN.py")
EchoStateNet = importlib.util.module_from_spec(spec2)
spec2.loader.exec_module(EchoStateNet)


skip_dim = 1


class ROM():

    param = [0.95, -0.76095, 0.1, 1.25, 0.2, 0.5]

    x1 = 1.0
    x2 = 1.0
    x3 = 1.0
    x4 = 1.0
    x5 = 1.0
    x6 = 1.0
    x0 = [[x1, x2, x3, x4, x5, x6]]

    def alpha(self,m):

        return (8*np.sqrt(2)*m*m*(self.param[5]*self.param[5]+m*m-1))/(np.pi*(4*m*m-1)*(self.param[5]*self.param[5]+m*m))

    def beta(self,m):

        return (self.param[3]*self.param[5]*self.param[5])/(self.param[5]*self.param[5]+m*m)

    def delta(self,m):

        return (64*np.sqrt(2)*(self.param[5]*self.param[5]-m*m+1))/(15*np.pi*(self.param[5]*self.param[5]+m*m))

    def gamma_star(self,m):

        return self.param[4]*(4*np.sqrt(2)*m*self.param[5])/(np.pi*(4*m*m-1))

    def epsilon(self):

        return 16*np.sqrt(2)/(5*np.pi)

    def gamma_m(self,m):

        return self.param[4]*(4*np.sqrt(2)*m*m*m*self.param[5])/(np.pi*(4*m*m-1)*(m*m+self.param[5]*self.param[5]))


    def dynamics(self, a, phi):

        #assert x.size == 3, 'Input needs 3 entries'

        x = np.matmul(a, np.transpose(phi))

        dxdt = np.zeros((6,))
        dxdt[0] = self.gamma_star(1)*x[2] - self.param[2]*(x[0]-self.param[0])
        dxdt[1] = -(self.alpha(1)*x[0]-self.beta(1))*x[2] - self.param[2]*x[1] - self.delta(1)*x[3]*x[5]
        dxdt[2] = (self.alpha(1)*x[0]-self.beta(1))*x[1] - self.gamma_m(1)*x[0] - self.param[2]*x[2] + self.delta(1)*x[3]*x[4]
        dxdt[3] = self.gamma_star(2)*x[5] - self.param[2]*(x[3]-self.param[1]) + self.epsilon()*(x[1]*x[5]-x[2]*x[4])
        dxdt[4] = -(self.alpha(2)*x[0]-self.beta(2))*x[5] - self.param[2]*x[4] - self.delta(2)*x[3]*x[2]
        dxdt[5] = (self.alpha(2)*x[0]-self.beta(2))*x[4] - self.gamma_m(2)*x[3] - self.param[2]*x[5] + self.delta(2)*x[3]*x[1]

        dadt = np.matmul(dxdt,phi)

        return dadt


    def integrate(self,a,phi,dt):

        #assert x.size == 3, 'Input needs 3 entries'

        k1 = self.dynamics(a,phi)
        k2 = self.dynamics(a+dt/2.0*k1,phi)
        k3 = self.dynamics(a+dt/2.0*k2,phi)
        k4 = self.dynamics(a+dt*k3,phi)

        return a + dt / 6.0 * (k1 + 2*k2 + 2*k3 + k4)



#setup
epsilon = 0.2
normalization = 0
# Read data from files
os.chdir("/Users/ml/Documents/Studium/Masterarbeit/Code/pemlm/CDV/")
np_file = np.load('./CDV_data01.npz')
X = np_file['X']  # Data
dt = np_file['dt']
t_split = np_file['t_stop_train']
t_skip = np_file['t_skip']
val_ratio = np_file['val_ratio']
Lyap = np_file['Lyap']



# prepare data for training

# skip beginning
i_skip = int(t_skip / dt)
X = X[i_skip:, :]

# normalize data
stddev = np.std(X, axis=0)
avg = np.average(X, axis=0)

if normalization == 0:
    X = (X - avg) / stddev

if normalization == 1:
    X = X - avg
    max = np.array([np.amax(np.absolute(X[:, 0])), np.amax(np.absolute(X[:, 1])), np.amax(np.absolute(X[:, 2]))])
    X = X / max

if normalization == 2:
    X = X - avg

#Decomposition
pod = propdec.POD()
eig, a, phi = pod(X,False)

#generate ROM
rom = ROM()

#reduce dimension
a_red = a[:,0:-skip_dim]
phi_red = phi[:,0:-skip_dim]

#create array with integrated ROM coefficients
#initialize ROM coefficients with first value
a_rom = [a_red[0,:]]
for i in range(a.shape[0]-1):
    a_rom.append(rom.integrate(a_red[i,:],phi_red,dt))

a_rom = np.array(a_rom)

#calculate error term
C = a_red - a_rom

#mapping a_red(true at t) --> C(at t+1)
input_all = a_red[:-1,:]
output_all = C[1:,:]


# split data into training, validation
idx_split = int(t_split / dt) - i_skip
assert idx_split > 0, 'skip time is bigger than split time'
# index that seperates training and validation data
idx_val = int(idx_split * (1 - val_ratio))

input_train = input_all[:idx_split, :]
output_train = output_all[:idx_split, :]


#training_idx = int(training_time/dt)
resync_time = 1
resync_idx = int(resync_time/(Lyap*dt))
washout = 100


#reshape input
input_train = input_train.reshape((1,input_train.shape[0],input_train.shape[1]))
print(input_train.shape)


#Create ESN with params
n_neurons = 500
rng = np.random.RandomState(1)
degree = 3.6  # average number of connections of a unit to other units in reservoir (3.0)
input_scaling = 0.37
rho = 0.6
leaking_rate = 1.0
beta = 0.00003

#create model with given parameters
sparseness = 1. - degree / (n_neurons - 1.)  # sparseness of W

model = EchoStateNet.ESN(n_neurons, input_train.shape[2], leaking_rate, rho, sparseness, input_scaling, rng, 'tanh', beta)

# train model
model.fit(input_train, output_train, washout)
model.reset_states()



#Predicition part
t_pred = 10 / Lyap
idx_end = int(t_pred / dt)
a_init = a_red[idx_split-resync_idx:idx_split,:]
x_ref = X[idx_split:, :]
assert idx_end < x_ref.shape[0], 't_pred too long'
x_ref = x_ref[:idx_end, :]

#initialize reservoir
a_init = a_init.reshape((1,a_init.shape[0],a_init.shape[1]))
model.predict(a_init)

C = []
a_pred = []

a_last = a_red[idx_split,:]
a_last = a_last.reshape((1, 1, a_last.shape[0]))

print('start natural response')
for i in range(x_ref.shape[0] - 1):
    # predict correction for next timestep
    C.append(model.predict(a_last))

    # integrate last coefficients
    a_int = rom.integrate(a_last[0, -1, :], phi_red, dt)

    # correct ROM model with predicted value
    a_pred.append(a_int + C[-1])

    # append last predicition to end of input
    a_last = a_pred[-1].reshape(1, a_pred[-1].shape[0], a_pred[-1].shape[1])

    print('(%d/%d) predicitions done' % (i, x_ref.shape[0])) if i % 50 == 0 and i != 0 else None

a_pred = np.array(a_pred)
a_pred = a_pred.reshape(a_pred.shape[0], a_pred.shape[2])

#add first timestep to array
a_pred = np.vstack((a_red[idx_split,:], a_pred))

# backtransformation
x_pred = np.matmul(a_pred, np.transpose(phi_red))

# denormalize
if normalization == 0:
    x_pred = x_pred * stddev + avg
    x_ref = x_ref * stddev + avg

if normalization == 1:
    x_pred = x_pred * max + avg
    x_ref = x_ref * max + avg

if normalization == 2:
    x_pred = x_pred + avg
    x_ref = x_ref + avg


# calculate Error
err = LA.norm(x_pred - x_ref, axis=1) / np.sqrt(
    np.average(np.square(LA.norm(x_ref, axis=1))))
plt.plot(err)
plt.show()

plt.plot(x_ref[:, 0])
plt.plot(x_pred[:, 0], '--')
plt.show()

plt.plot(x_ref[:, 1])
plt.plot(x_pred[:, 1], '--')
plt.show()

plt.plot(x_ref[:, 2])
plt.plot(x_pred[:, 2], '--')
plt.show()

plt.plot(x_ref[:, 3])
plt.plot(x_pred[:, 3], '--')
plt.show()

plt.plot(x_ref[:, 4])
plt.plot(x_pred[:, 4], '--')
plt.show()

plt.plot(x_ref[:, 5])
plt.plot(x_pred[:, 5], '--')
plt.show()