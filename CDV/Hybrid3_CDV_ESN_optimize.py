import os
import numpy as np
from tensorflow.keras import backend as K
import matplotlib.pyplot as plt
import numpy.linalg as LA
#import POD
import importlib.util
spec1 = importlib.util.spec_from_file_location("POD", "/Users/ml/Documents/Studium/Masterarbeit/Code/pemlm/POD.py")
propdec = importlib.util.module_from_spec(spec1)
spec1.loader.exec_module(propdec)

#import ESN
spec2 = importlib.util.spec_from_file_location("ESN", "/Users/ml/Documents/Studium/Masterarbeit/Code/pemlm/ESN/ESN_Hybrid.py")
EchoStateNet = importlib.util.module_from_spec(spec2)
spec2.loader.exec_module(EchoStateNet)


skip_dim = 1


class ROM():

    param = [0.95, -0.76095, 0.1, 1.25, 0.2, 0.5]

    x1 = 1.0
    x2 = 1.0
    x3 = 1.0
    x4 = 1.0
    x5 = 1.0
    x6 = 1.0
    x0 = [[x1, x2, x3, x4, x5, x6]]

    def alpha(self,m):

        return (8*np.sqrt(2)*m*m*(self.param[5]*self.param[5]+m*m-1))/(np.pi*(4*m*m-1)*(self.param[5]*self.param[5]+m*m))

    def beta(self,m):

        return (self.param[3]*self.param[5]*self.param[5])/(self.param[5]*self.param[5]+m*m)

    def delta(self,m):

        return (64*np.sqrt(2)*(self.param[5]*self.param[5]-m*m+1))/(15*np.pi*(self.param[5]*self.param[5]+m*m))

    def gamma_star(self,m):

        return self.param[4]*(4*np.sqrt(2)*m*self.param[5])/(np.pi*(4*m*m-1))

    def epsilon(self):

        return 16*np.sqrt(2)/(5*np.pi)

    def gamma_m(self,m):

        return self.param[4]*(4*np.sqrt(2)*m*m*m*self.param[5])/(np.pi*(4*m*m-1)*(m*m+self.param[5]*self.param[5]))


    def dynamics(self, a, phi):

        #assert x.size == 3, 'Input needs 3 entries'

        x = np.matmul(a, np.transpose(phi))

        dxdt = np.zeros((6,))
        dxdt[0] = self.gamma_star(1)*x[2] - self.param[2]*(x[0]-self.param[0])
        dxdt[1] = -(self.alpha(1)*x[0]-self.beta(1))*x[2] - self.param[2]*x[1] - self.delta(1)*x[3]*x[5]
        dxdt[2] = (self.alpha(1)*x[0]-self.beta(1))*x[1] - self.gamma_m(1)*x[0] - self.param[2]*x[2] + self.delta(1)*x[3]*x[4]
        dxdt[3] = self.gamma_star(2)*x[5] - self.param[2]*(x[3]-self.param[1]) + self.epsilon()*(x[1]*x[5]-x[2]*x[4])
        dxdt[4] = -(self.alpha(2)*x[0]-self.beta(2))*x[5] - self.param[2]*x[4] - self.delta(2)*x[3]*x[2]
        dxdt[5] = (self.alpha(2)*x[0]-self.beta(2))*x[4] - self.gamma_m(2)*x[3] - self.param[2]*x[5] + self.delta(2)*x[3]*x[1]

        dadt = np.matmul(dxdt,phi)

        return dadt


    def integrate(self,a,phi,dt):

        #assert x.size == 3, 'Input needs 3 entries'

        k1 = self.dynamics(a,phi)
        k2 = self.dynamics(a+dt/2.0*k1,phi)
        k3 = self.dynamics(a+dt/2.0*k2,phi)
        k4 = self.dynamics(a+dt*k3,phi)

        return a + dt / 6.0 * (k1 + 2*k2 + 2*k3 + k4)



#setup
epsilon = 0.4
normalization = 0
# Read data from files
os.chdir("/Users/ml/Documents/Studium/Masterarbeit/Code/pemlm/CDV/")
np_file = np.load('./CDV_data01.npz')
X = np_file['X']  # Data
dt = np_file['dt']
t_split = np_file['t_stop_train']
t_skip = np_file['t_skip']
val_ratio = np_file['val_ratio']
Lyap = np_file['Lyap']



#initial values
n_neurons = 500
degree = 3.6  # average number of connections of a unit to other units in reservoir (3.0)
input_scaling = 0.37
rho = 0.6
leaking_rate = 1.0
beta = 0.00003

#dictionary for first parameter search
dict = {
    "input_scaling": [0.2, 0.25, 0.3, 0.37, 0.4],
    "rho": [0.4, 0.5, 0.6, 0.7],
    "degree": [3.0, 3.6, 4.0],
    "leaking_rate": [0.9, 0.95, 1.0],
    "beta": [0.00001, 0.00003, 0.00005]
}

#dictionary for initial step size in parameter search (half of difference in parameters)
dict_step = {
    "input_scaling": 0.01,
    "rho": 0.05,
    "degree": 0.2,
    "leaking_rate": 0.02,
    "beta": 0.00001
}


# prepare data for training

# skip beginning
i_skip = int(t_skip / dt)
X = X[i_skip:, :]

# normalize data
stddev = np.std(X, axis=0)
avg = np.average(X, axis=0)

if normalization == 0:
    X = (X - avg) / stddev

if normalization == 1:
    X = X - avg
    max = np.array([np.amax(np.absolute(X[:, 0])), np.amax(np.absolute(X[:, 1])), np.amax(np.absolute(X[:, 2]))])
    X = X / max

if normalization == 2:
    X = X - avg

#Decomposition
pod = propdec.POD()
eig, a, phi = pod(X, False)

# generate ROM
rom = ROM()

# reduce dimension
a_red = a[:, 0:-skip_dim]
phi_red = phi[:, 0:-skip_dim]

# create array with integrated ROM coefficients
# initialize ROM coefficients with first value
a_rom = []
for i in range(a.shape[0]):
    a_rom.append(rom.integrate(a_red[i, :], phi_red, dt))

a_rom = np.array(a_rom)

# calculate x_rom
x_rom = np.matmul(a_rom, np.transpose(phi_red))

# Concatenate real solution
input_all = np.concatenate((x_rom, X), axis=1)

# Create mapping
input_all = input_all[:-1, :]
output_all = X[1:, :]


# split data into training, validation
idx_split = int(t_split / dt) - i_skip
assert idx_split > 0, 'skip time is bigger than split time'
# index that seperates training and validation data
idx_val = int(idx_split * (1 - val_ratio))

input_train = input_all[:idx_split, :]
output_train = output_all[:idx_split, :]


#training_idx = int(training_time/dt)
resync_time = 1
resync_idx = int(resync_time/(Lyap*dt))
washout = 100


#shorten training data
#input_train = input_train[:training_idx,:]
#output_train = output_train[:training_idx,:]

#reshape input
input_train = input_train.reshape((1,input_train.shape[0],input_train.shape[1]))
print(input_train.shape)

#create dict for best parameters
best_param = {}

if normalization == None:
    file = open("CDV_ESN_Hybrid3_unnorm_" + str(epsilon) + ".txt", "a")
else:
    file = open("CDV_ESN_Hybrid3_" + str(epsilon) +".txt", "a")

for name,val in dict.items():

    file.write("Start parameter search with parameter: " + name + "\n")
    print("Start parameter search with parameter: " + name)

    results = []
    results_std = []
    iter = 0

    for value in val:

        exec("%s = %f" % (name,value))

        #create model with given parameters
        sparseness = 1. - degree / (n_neurons - 1.)  # sparseness of W

        K.clear_session()
        rng = np.random.RandomState(1)

        model = EchoStateNet.ESN_Hybrid(n_neurons, input_train.shape[2], leaking_rate, rho, sparseness, input_scaling, rng, 'tanh', beta)

        #train model
        model.fit(input_train, output_train, washout)
        model.reset_states()

        print("model with: " + str(n_neurons) + " neurons, " + str(sparseness) + " sparseness, " +
                   str(leaking_rate) + " leaking_rate, " + str(rho) + " rho, " + str(input_scaling) + " input_scaling, "
                   + str(beta) + " beta")
        file.write("model with: " + str(n_neurons) + " neurons, " + str(sparseness) + " sparseness, " +
                   str(leaking_rate) + " leaking_rate, " + str(rho) + " rho, " + str(input_scaling) + " input_scaling, "
                   + str(beta) + " beta" + "\n")

        #reference solution for prediction section
        x_ref = X[idx_split:, :]

        t_pred = 10 / Lyap
        err_t = []

        # start prediction
        for i in range(50):

            idx_start = int(i * t_pred / dt)
            idx_end = int((i + 1) * t_pred / dt)

            x_init = input_all[idx_split + idx_start - resync_idx:idx_split + idx_start , :]

            assert idx_end < x_ref.shape[0], 't_pred too long'
            x_loc = x_ref[idx_start:idx_end, :]  # local reference solution


            # initialize reservoir
            x_init = x_init.reshape((1, x_init.shape[0], x_init.shape[1]))
            model.predict(x_init)

            x_pred = []

            x_last = input_all[idx_split + idx_start, :]
            x_last = x_last.reshape((1, 1, x_last.shape[0]))

            print('start natural response')
            for i in range(x_loc.shape[0] - 1):
                # predict correction for next timestep
                x_pred.append(model.predict(x_last))

                # compute POD coefficients
                a_last = np.matmul(x_pred[-1], phi_red)

                # integrate last coefficients
                a_last = rom.integrate(a_last[0, :], phi_red, dt)

                # add zero to end of a_int
                x_new = np.matmul(a_last, np.transpose(phi_red))

                x_last = np.concatenate((x_new, x_pred[-1][0, :]), axis=0)

                # reshape x_last
                x_last = x_last.reshape(1, 1, x_last.shape[0])

                print('(%d/%d) predicitions done' % (i, x_loc.shape[0])) if i % 50 == 0 and i != 0 else None


            x_pred = np.array(x_pred)
            x_pred = x_pred.reshape(x_pred.shape[0], x_pred.shape[2])

            # add first timestep to array
            x_pred = np.vstack((X[idx_split + idx_start, :], x_pred))

            model.reset_states()

            # denormalize
            if normalization == 0:
                x_pred = x_pred * stddev + avg
                x_loc = x_loc * stddev + avg

            if normalization == 1:
                x_pred = x_pred * max + avg
                x_loc = x_loc * max + avg

            if normalization == 2:
                x_pred = x_pred + avg
                x_loc = x_loc + avg

            # calculate Error
            err = LA.norm(x_pred - x_loc, axis=1) / np.sqrt(
                np.average(np.square(LA.norm(x_loc, axis=1))))

            t = (np.argmax(err > epsilon) + 1) * dt
            err_t.append(t)

        err_t = np.array(err_t)
        print('mean:%f' % np.average(err_t))
        print('standarddeviation:%f' % np.std(err_t))

        #write to file
        file.write("mean:" + str(np.average(err_t)) + "\n")
        file.write("stddev:" + str(np.std(err_t)) + "\n")

        results.append(np.average(err_t))
        results_std.append(np.std(err_t))


    if iter == 0:
        # store and set best value
        idx = results.index(max(results))
        best_param[name] = val[idx]
        err_old = max(results)
        std_old = results_std[idx]
        exec("%s = %f" % (name, val[idx]))

        print("chose" + name + "=" + str(val[idx]) + "as best value")
        file.write("chose" + name + "=" + str(val[idx]) + "as best value" + "\n")

    if iter != 0 and max(results) > err_old:
        # store and set best value
        idx = results.index(max(results))
        best_param[name] = val[idx]
        err_old = max(results)
        std_old = results_std[idx]
        exec("%s = %f" % (name, val[idx]))

        print("chose" + name + "=" + str(val[idx]) + "as best value")
        file.write("chose" + name + "=" + str(val[idx]) + "as best value" + "\n")

    if iter != 0 and max(results) < err_old:
        print("Run: " + name + " ,no better value found.")
        file.write("Run: " + name + ", no better value found." + "\n")


    iter += 1


print(best_param)


####################################################################################
# Line Search

for name,val in dict_step.items():

    #maximum values
    max_iter = 14
    max_refinement = 4
    refinement_factor = 0.1

    #set initial best value
    best_val = best_param[name]


    for direction in [-1,1]:

        file.write("Start line search in direction: " + str(direction) + " with parameter: " + name + "\n")
        print("Start line search in direction: " + str(direction) + " with parameter: " + name)

        #initial values
        step = val
        exec("%s = %f" % (name, best_param[name]))
        refinement = 0
        iter = 0

        exec("%s = %s + %f * %d" % (name, name, step, direction))

        while refinement < max_refinement and iter < max_iter:


            # create model with given parameters
            sparseness = 1. - degree / (n_neurons - 1.)  # sparseness of W

            K.clear_session()
            rng = np.random.RandomState(1)

            model = EchoStateNet.ESN_Hybrid(n_neurons, input_train.shape[2], leaking_rate, rho, sparseness, input_scaling, rng, 'tanh', beta)

            # train model
            model.fit(input_train, output_train, washout)
            model.reset_states()

            print("model with: " + str(n_neurons) + " neurons, " + str(sparseness) + " sparseness, " +
                  str(leaking_rate) + " leaking_rate, " + str(rho) + " rho, " + str(input_scaling) + " input_scaling, "
                  + str(beta) + " beta")
            file.write("model with: " + str(n_neurons) + " neurons, " + str(sparseness) + " sparseness, " +
                       str(leaking_rate) + " leaking_rate, " + str(rho) + " rho, " + str(
                input_scaling) + " input_scaling, "
                       + str(beta) + " beta" + "\n")

            # reference solution for prediction section
            x_ref = X[idx_split:, :]

            t_pred = 10 / Lyap
            err_t = []

            # start prediction
            for i in range(50):

                idx_start = int(i * t_pred / dt)
                idx_end = int((i + 1) * t_pred / dt)

                x_init = input_all[idx_split + idx_start - resync_idx:idx_split + idx_start, :]

                assert idx_end < x_ref.shape[0], 't_pred too long'
                x_loc = x_ref[idx_start:idx_end, :]  # local reference solution

                # initialize reservoir
                x_init = x_init.reshape((1, x_init.shape[0], x_init.shape[1]))
                model.predict(x_init)

                x_pred = []

                x_last = input_all[idx_split + idx_start, :]
                x_last = x_last.reshape((1, 1, x_last.shape[0]))

                print('start natural response')
                for i in range(x_loc.shape[0] - 1):
                    # predict correction for next timestep
                    x_pred.append(model.predict(x_last))

                    # compute POD coefficients
                    a_last = np.matmul(x_pred[-1], phi_red)

                    # integrate last coefficients
                    a_last = rom.integrate(a_last[0, :], phi_red, dt)

                    # add zero to end of a_int
                    x_new = np.matmul(a_last, np.transpose(phi_red))

                    x_last = np.concatenate((x_new, x_pred[-1][0, :]), axis=0)

                    # reshape x_last
                    x_last = x_last.reshape(1, 1, x_last.shape[0])

                    print('(%d/%d) predicitions done' % (i, x_loc.shape[0])) if i % 50 == 0 and i != 0 else None

                x_pred = np.array(x_pred)
                x_pred = x_pred.reshape(x_pred.shape[0], x_pred.shape[2])

                # add first timestep to array
                x_pred = np.vstack((X[idx_split + idx_start, :], x_pred))

                model.reset_states()

                # denormalize
                if normalization == 0:
                    x_pred = x_pred * stddev + avg
                    x_loc = x_loc * stddev + avg

                if normalization == 1:
                    x_pred = x_pred * max + avg
                    x_loc = x_loc * max + avg

                if normalization == 2:
                    x_pred = x_pred + avg
                    x_loc = x_loc + avg

                # calculate Error
                err = LA.norm(x_pred - x_loc, axis=1) / np.sqrt(
                    np.average(np.square(LA.norm(x_loc, axis=1))))

                t = (np.argmax(err > epsilon) + 1) * dt
                err_t.append(t)


            err_t = np.array(err_t)
            print('mean:%f' % np.average(err_t))
            print('standarddeviation:%f' % np.std(err_t))

            # write to file
            file.write("mean:" + str(np.average(err_t)) + "\n")
            file.write("stddev:" + str(np.std(err_t)) + "\n")

            err_new = np.average(err_t)
            std_new = np.std(err_t)


            if err_new < err_old:

                step = step * refinement_factor
                refinement = refinement + 1
                exec("%s = %s + %f * %d" % (name, name, step, direction))

                print("Param %s: go again in this direction with smaller step size" % name)
                file.write("Param " + name + ": go again in this direction with smaller step size" + "\n")

            if err_new >= err_old:
                exec("best_val = %s" % name)

                exec("%s = %s + %f * %d" % (name, name, step, direction))

                print("Param %s: go further in this direction with same step size" % name)
                file.write("Param " + name + ": go further in this direction with same step size" + "\n")
                err_old = err_new
                std_old = std_new

            iter += 1



    #Set best value
    exec("%s = best_val" % name)
    #store it
    best_param[name] = best_val
    #exec(str(best_param[name]) + "= %s" % name)

    print("chose " + name + "=" + str(best_param[name]) + "as best value") #TODO writes wrong output
    file.write("chose " + name + "=" + str(best_param[name]) + "as best value" + "\n")

file.write("best parameters:" + "\n")
for name,val in best_param.items():

    file.write(name + " = " + str(val) + "\n")
    print(name + " = " + str(val))

file.write("average time:" + "\n")
file.write(str(err_old) + "\n")

print("average time:")
print(err_old)

file.write("stddev time:" + "\n")
file.write(str(std_old) + "\n")

print("stddev time:")
print(std_old)

file.close()

# plt.plot(err)
# plt.show()
#
# plt.plot(x_ref[:, 0])
# plt.plot(Y[:, 0], '--')
# plt.show()
#
# plt.plot(x_ref[:, 1])
# plt.plot(Y[:, 1], '--')
# plt.show()
#
# plt.plot(x_ref[:, 2])
# plt.plot(Y[:, 2], '--')
# plt.show()
