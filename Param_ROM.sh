#!/bin/bash

units=(4 5 6 7)
lookback=($(seq 3 6 16))
epochs=(100 150)


for i in "${units[@]}"; do
  for j in "${lookback[@]}"; do
    for k in "${epochs[@]}"; do
      echo "perform unit=$i, lookback=$j, epochs=$k simulation"
      python3 ./LorenzSys/ErrPred.py -u $i -l $j -e $k
    done
  done
done


